export const environment = {
  production: true,
  baseUrl: 'https://expenses.fsquiroz.com',
  pageSize: 20,
  defaultDateFormat: 'yyyy/MM/dd HH:mm:ss',
  version: '1.13.2'
};
