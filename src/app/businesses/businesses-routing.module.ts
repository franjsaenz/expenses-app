import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {BusinessesPage} from './businesses.page';
import {AuthService} from '../services/auth.service';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthService],
    component: BusinessesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BusinessesPageRoutingModule {}
