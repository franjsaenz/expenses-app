import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {BusinessesPageRoutingModule} from './businesses-routing.module';

import {BusinessesPage} from './businesses.page';
import {BusinessDetailComponent} from '../modal/business-detail/business-detail.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BusinessesPageRoutingModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    TranslateModule
  ],
  declarations: [BusinessesPage, BusinessDetailComponent]
})
export class BusinessesPageModule {
}
