import {Component, OnInit, ViewChild} from '@angular/core';
import {IonInfiniteScroll} from '@ionic/angular';
import {Sort, SortConfig} from '../component/sort-popover/sort-popover.component';
import {Business} from '../entity/business';
import {ErrorMappingService} from '../services/error-mapping.service';
import {BusinessService} from '../services/business.service';
import {Page} from '../entity/page';
import {HttpErrorResponse} from '@angular/common/http';
import {Subscription} from 'rxjs';
import {BusinessDetailComponent} from '../modal/business-detail/business-detail.component';
import {ModalService} from '../services/modal.service';
import {PopoverService} from '../services/popover.service';
import {LanguageService} from '../services/language.service';

@Component({
  selector: 'app-businesses',
  templateUrl: './businesses.page.html',
  styleUrls: ['./businesses.page.scss'],
})
export class BusinessesPage implements OnInit {

  public loadingContent = true;

  public searchTerm: string;

  public pageNumber = 1;

  public businesses: Array<Business> = [];

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  public sortConfig: SortConfig;

  constructor(
    private businessService: BusinessService,
    private errorMappingService: ErrorMappingService,
    private modalService: ModalService,
    private popoverService: PopoverService,
    private languageService: LanguageService
  ) {
    languageService.setTitle('business.plural');
  }

  ngOnInit() {
    this.sortConfig = this.businessService.getBusinessSortingConfig();
    this.listContent();
  }

  listContent() {
    this.loadingContent = true;
    if (this.infiniteScroll != null) {
      this.infiniteScroll.disabled = true;
    }
    this.businessService.search(
      this.searchTerm, this.pageNumber - 1,
      this.sortConfig.currentSort.property,
      this.sortConfig.currentSort.ascending
    ).subscribe((resp: Page<Business>) => {
      this.loadingContent = false;
      this.businesses = this.businesses.concat(resp.content);
      this.pageNumber++;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = resp.last;
    }, (err: HttpErrorResponse) => {
      this.loadingContent = false;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = true;
      this.errorMappingService.displayError(err.error, ErrorMappingService.WARNING);
    });
  }

  search(term: string) {
    this.resetList();
    if (term == null || term.length < 1) {
      this.searchTerm = null;
    } else {
      this.searchTerm = term;
    }
    this.listContent();
  }

  resetList() {
    this.searchTerm = null;
    this.businesses = [];
    this.pageNumber = 1;
  }

  async openBusiness(bus: Business, index: number) {
    const response = await this.modalService.presentModal(BusinessDetailComponent, {business: bus});
    if (response == null || index == null || index < 0) {
      this.businesses = [];
      this.pageNumber = 1;
      this.listContent();
    } else {
      this.businesses[index] = response;
    }
  }

  async presentSort(ev: Event) {
    const sortSub: Subscription = this.businessService.subscribeToSort((sort: Sort) => {
      this.sortConfig.currentSort = sort;
      this.sortListing();
    });
    await this.popoverService.presentSort(ev, this.sortConfig, sortSub);
  }

  sortListing(): void {
    this.businesses = [];
    this.pageNumber = 1;
    this.listContent();
  }

}
