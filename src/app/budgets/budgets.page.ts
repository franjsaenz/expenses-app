import {Component, OnInit, ViewChild} from '@angular/core';
import {IonInfiniteScroll} from '@ionic/angular';
import {Sort, SortConfig} from '../component/sort-popover/sort-popover.component';
import {ErrorMappingService} from '../services/error-mapping.service';
import {Page} from '../entity/page';
import {HttpErrorResponse} from '@angular/common/http';
import {Budget} from '../entity/budget';
import {BudgetService} from '../services/budget.service';
import {environment} from '../../environments/environment';
import {User} from '../entity/user';
import {AuthService} from '../services/auth.service';
import {SettingService} from '../services/setting.service';
import {Router} from '@angular/router';
import {BudgetCreateComponent} from '../modal/budget-create/budget-create.component';
import {ModalService} from '../services/modal.service';
import {PopoverService} from '../services/popover.service';
import {LanguageService} from '../services/language.service';

@Component({
  selector: 'app-budgets',
  templateUrl: './budgets.page.html',
  styleUrls: ['./budgets.page.scss'],
})
export class BudgetsPage implements OnInit {

  public dateFormat: string = environment.defaultDateFormat;

  public loadingContent = true;

  public searchTerm: string;

  public searchWithin: string;

  public pageNumber = 1;

  public budgets: Array<Budget> = [];

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  public sortConfig: SortConfig;

  constructor(
    private budgetService: BudgetService,
    private authService: AuthService,
    private settingService: SettingService,
    private errorMappingService: ErrorMappingService,
    private modalService: ModalService,
    private popoverService: PopoverService,
    private languageService: LanguageService,
    private router: Router
  ) {
    languageService.setTitle('budget.plural');
  }

  ngOnInit() {
    this.sortConfig = this.budgetService.getBudgetSortingConfig();
    this.authService.getLoggedIn().then((user: User) => {
      this.dateFormat = this.settingService.getDateFormat(user.dateFormat).pipe;
    });
  }

  ionViewWillEnter() {
    this.budgets = [];
    this.pageNumber = 1;
    this.listContent();
  }

  listContent() {
    this.loadingContent = true;
    if (this.infiniteScroll != null) {
      this.infiniteScroll.disabled = true;
    }
    this.budgetService.search(
      this.searchTerm, null,
      this.pageNumber - 1,
      this.sortConfig.currentSort.property,
      this.sortConfig.currentSort.ascending
    ).subscribe((resp: Page<Budget>) => {
      this.loadingContent = false;
      this.budgets = this.budgets.concat(resp.content);
      this.pageNumber++;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = resp.last;
    }, (err: HttpErrorResponse) => {
      this.loadingContent = false;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = true;
      this.errorMappingService.displayError(err.error, ErrorMappingService.WARNING);
    });
  }

  search(term: string) {
    this.resetList();
    if (term == null || term.length < 1) {
      this.searchTerm = null;
    } else {
      this.searchTerm = term;
    }
    this.listContent();
  }

  resetList() {
    this.searchTerm = null;
    this.searchWithin = null;
    this.budgets = [];
    this.pageNumber = 1;
  }

  openBudget(bgt: Budget) {
    this.router.navigate(['budgets', 'detail', bgt.id]);
  }

  async createBudget() {
    const response = await this.modalService.presentModal(BudgetCreateComponent, {toCopy: null});
    if (response != null) {
      return this.openBudget(response);
    }
  }

  async presentSort(ev: Event) {
    await this.popoverService.presentSort(ev, this.sortConfig, this.budgetService.subscribeToSort((sort: Sort) => {
      this.sortConfig.currentSort = sort;
      this.sortListing();
    }));
  }

  sortListing(): void {
    this.budgets = [];
    this.pageNumber = 1;
    this.listContent();
  }

}
