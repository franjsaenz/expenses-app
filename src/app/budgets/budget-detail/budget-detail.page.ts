import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Budget} from '../../entity/budget';
import {SortConfig} from '../../component/sort-popover/sort-popover.component';
import {AuthService} from '../../services/auth.service';
import {ErrorMappingService} from '../../services/error-mapping.service';
import {SettingService} from '../../services/setting.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {BudgetService} from '../../services/budget.service';
import {User} from '../../entity/user';
import {HttpErrorResponse} from '@angular/common/http';
import {Currency} from '../../entity/currency';
import {CurrencySearchComponent} from '../../modal/currency-search/currency-search.component';
import {BudgetReport} from '../../entity/budget-report';
import {BudgetCreateComponent} from '../../modal/budget-create/budget-create.component';
import {ChartService} from '../../services/chart.service';
import {Chart} from 'chart.js';
import {ModalService} from '../../services/modal.service';
import {PopoverService} from '../../services/popover.service';
import {ActionSheetService} from '../../services/action-sheet.service';
import {Location} from '@angular/common';
import {LanguageService} from '../../services/language.service';
import {DateService} from '../../services/date.service';
import {CopyTextComponent} from '../../modal/copy-text/copy-text.component';

@Component({
  selector: 'app-budget-detail',
  templateUrl: './budget-detail.page.html',
  styleUrls: ['./budget-detail.page.scss'],
})
export class BudgetDetailPage implements OnInit {

  dateFormat: string;

  readonly maxDate: string = new Date(new Date().getFullYear() + 2, 1, 1).toISOString();

  private id: number;

  editing = false;

  isSending = false;

  budgetReport: BudgetReport;

  selectedCurrency: Currency;

  @ViewChild('budgetGraph') graphCanvas: ElementRef;

  graph: Chart;

  graphAttempts = 0;

  form: FormGroup;

  segment = 'info';

  itemSortConfig: SortConfig;

  constructor(
    private budgetService: BudgetService,
    private authService: AuthService,
    private errorMappingService: ErrorMappingService,
    private settingService: SettingService,
    private chartService: ChartService,
    private modalService: ModalService,
    private popoverService: PopoverService,
    private actionSheetService: ActionSheetService,
    private languageService: LanguageService,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private location: Location
  ) {
    languageService.setTitle('budget.detailTitle');
    const id: any = activatedRoute.snapshot.params.budgetId;
    if (id != null) {
      try {
        this.id = +id;
      } catch (e) {
        console.error(e);
        this.id = null;
      }
    }
    this.itemSortConfig = budgetService.getItemsSortingConfig();
  }

  ngOnInit() {
    if (this.id == null || isNaN(this.id) || this.id < 1) {
      this.close();
      return;
    }
    this.authService.getLoggedIn().then((user: User) => {
      this.dateFormat = this.settingService.getDateFormat(user.dateFormat).selector;
    });
    this.refreshBudget();
  }

  segmentChanged(newSegment: string) {
    if ('info' === newSegment) {
      this.refreshBudget();
    } else {
      this.graph = null;
      this.cancelEdit();
    }
  }

  private refreshBudget() {
    this.isSending = true;
    this.budgetService.getExpanded(this.id).subscribe(
      (report: BudgetReport) => {
        this.budgetReport = report;
        this.selectedCurrency = report.budget.currency;
        this.isSending = false;
        this.graph = null;
        this.initForm();
        this.initGraph();
      },
      (error: HttpErrorResponse) => {
        this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
        this.close();
      }
    );
  }

  private initForm() {
    this.form = this.formBuilder.group({
      id: new FormControl(this.budgetReport.budget.id),
      name: new FormControl(this.budgetReport.budget.name, [
        Validators.required
      ]),
      from: new FormControl(this.budgetReport.budget.from, [
        Validators.required
      ]),
      to: new FormControl(this.budgetReport.budget.to, [
        Validators.required
      ]),
      currency: new FormControl(this.selectedCurrency.name, [
        Validators.required
      ])
    });
    this.form.get('id').disable();
    this.form.get('name').disable();
    this.form.get('from').disable();
    this.form.get('to').disable();
    this.form.get('currency').disable();
  }

  private initGraph() {
    if ('info' !== this.segment) {
      return;
    }
    if (this.graphCanvas != null && this.budgetReport != null) {
      this.chartService.createBudgetChart(this.budgetReport, this.graphCanvas).then(graph => {
        this.graph = graph;
      });
      this.graphAttempts = 0;
    } else {
      this.graphAttempts++;
      setTimeout(() => {
        if (this.graphAttempts > 10) {
          console.error('Unable to create Graph: ', {graph: this.graph, canvas: this.graphCanvas, report: this.budgetReport});
        } else {
          this.initGraph();
        }
      }, 500);
    }
  }

  toggleBarLabels() {
    this.graph.options.scales.yAxes[0].display = !this.graph.options.scales.yAxes[0].display;
    this.graph.update();
  }

  startEdit() {
    this.editing = true;
    this.form.get('name').enable();
    this.form.get('name').markAsTouched();
    this.form.get('from').enable();
    this.form.get('from').markAsTouched();
    this.form.get('to').enable();
    this.form.get('to').markAsTouched();
    this.form.get('currency').enable();
    this.form.get('currency').markAsTouched();
  }

  cancelEdit() {
    this.editing = false;
    this.selectedCurrency = this.budgetReport.budget.currency;
    this.form.get('id').disable();
    this.form.get('name').setValue(this.budgetReport.budget.name);
    this.form.get('name').disable();
    this.form.get('from').setValue(this.budgetReport.budget.from);
    this.form.get('from').disable();
    this.form.get('to').setValue(this.budgetReport.budget.to);
    this.form.get('to').disable();
    this.form.get('currency').setValue(this.selectedCurrency.name);
    this.form.get('currency').disable();
    this.initGraph();
  }

  save() {
    const toSend: Budget = {
      id: this.budgetReport.budget.id,
      name: this.form.get('name').value,
      from: DateService.truncateMillis(this.form.get('from').value),
      to: DateService.truncateMillis(this.form.get('to').value),
    };
    this.isSending = true;
    this.editing = false;
    this.budgetService.edit(toSend, this.selectedCurrency).subscribe(
      (ignored: Budget) => {
        this.isSending = false;
        this.cancelEdit();
        this.refreshBudget();
      }, (error: HttpErrorResponse) => {
        this.isSending = false;
        this.editing = true;
        this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
      }
    );
  }

  async delete() {
    const handler = () => {
      this.isSending = true;
      this.budgetService.delete(this.budgetReport.budget.id).subscribe(
        () => {
          this.close();
        }, (error: HttpErrorResponse) => {
          this.isSending = false;
          this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
        }
      );
    };
    await this.actionSheetService.presentDelete('budget.delete', this.budgetReport.budget.name,
      'general.delete', 'general.cancel', handler
    );
  }

  async copy() {
    const response = await this.modalService.presentModal(BudgetCreateComponent, {toCopy: this.budgetReport.budget});
    if (response != null) {
      this.id = response.id;
      this.refreshBudget();
    }
  }

  async copyBudget() {
    let toCopy = 'Category\tExpected\tExpended';
    this.budgetReport.details.forEach(e => {
      const expected = (e.expected + '').replace('.', ',');
      const expended = (e.expended + '').replace('.', ',');
      toCopy += '\n';
      toCopy += e.name + '\t' + expected + '\t' + expended;
    });
    await this.modalService.presentModal(CopyTextComponent, {toCopy});
  }

  close() {
    this.location.back();
  }

  async searchCurrency() {
    const response = await this.modalService.presentModal(CurrencySearchComponent);
    if (response != null) {
      this.selectedCurrency = response;
      this.form.get('currency').setValue(this.selectedCurrency.name);
    }
  }

  async presentItemSort(ev: Event) {
    await this.popoverService.presentSort(ev, this.itemSortConfig);
  }

}
