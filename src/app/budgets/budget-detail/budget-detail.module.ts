import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {BudgetDetailPageRoutingModule} from './budget-detail-routing.module';

import {BudgetDetailPage} from './budget-detail.page';
import {BudgetItemDetailsComponent} from './budget-item-details/budget-item-details.component';
import {BudgetItemDetailComponent} from '../../modal/budget-item-detail/budget-item-detail.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {BudgetCreateComponent} from '../../modal/budget-create/budget-create.component';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BudgetDetailPageRoutingModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    TranslateModule
  ],
  declarations: [
    BudgetDetailPage,
    BudgetItemDetailsComponent,
    BudgetItemDetailComponent,
    BudgetCreateComponent
  ]
})
export class BudgetDetailPageModule {
}
