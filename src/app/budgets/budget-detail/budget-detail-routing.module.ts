import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {BudgetDetailPage} from './budget-detail.page';
import {AuthService} from '../../services/auth.service';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthService],
    component: BudgetDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BudgetDetailPageRoutingModule {}
