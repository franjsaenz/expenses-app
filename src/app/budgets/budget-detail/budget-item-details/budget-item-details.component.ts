import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {IonInfiniteScroll} from '@ionic/angular';
import {Sort, SortConfig} from '../../../component/sort-popover/sort-popover.component';
import {Subscription} from 'rxjs';
import {Budget} from '../../../entity/budget';
import {BudgetItem} from '../../../entity/budget-item';
import {BudgetService} from '../../../services/budget.service';
import {ErrorMappingService} from '../../../services/error-mapping.service';
import {Page} from '../../../entity/page';
import {HttpErrorResponse} from '@angular/common/http';
import {BudgetItemDetailComponent} from '../../../modal/budget-item-detail/budget-item-detail.component';
import {ModalService} from '../../../services/modal.service';

@Component({
  selector: 'app-budget-item-details',
  templateUrl: './budget-item-details.component.html',
  styleUrls: ['./budget-item-details.component.scss'],
})
export class BudgetItemDetailsComponent implements OnInit, OnDestroy {

  @Input() budget: Budget;

  public loadingContent = true;

  public pageNumber = 1;

  public items: Array<BudgetItem> = [];

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  public sortConfig: SortConfig;

  private sortSub: Subscription;

  constructor(
    private budgetService: BudgetService,
    private errorMappingService: ErrorMappingService,
    private modalService: ModalService
  ) {
  }

  ngOnInit() {
    this.sortConfig = this.budgetService.getItemsSortingConfig();
    this.sortSub = this.budgetService.subscribeToItemSort((sort: Sort) => {
      this.sortConfig.currentSort = sort;
      this.sortListing();
    });
    this.listContent();
  }

  listContent() {
    this.loadingContent = true;
    if (this.infiniteScroll != null) {
      this.infiniteScroll.disabled = true;
    }
    this.budgetService.listItems(
      this.budget, this.pageNumber - 1,
      this.sortConfig.currentSort.property,
      this.sortConfig.currentSort.ascending
    ).subscribe((resp: Page<BudgetItem>) => {
      this.loadingContent = false;
      this.items = this.items.concat(resp.content);
      this.pageNumber++;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = resp.last;
    }, (err: HttpErrorResponse) => {
      this.loadingContent = false;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = true;
      this.errorMappingService.displayError(err.error, ErrorMappingService.WARNING);
    });
  }

  ngOnDestroy() {
    this.sortConfig.publisher.next({property: null, ascending: null});
    this.sortSub.unsubscribe();
  }

  resetList() {
    this.items = [];
    this.pageNumber = 1;
  }

  async openDiscount(it: BudgetItem, index: number) {
    await this.modalService.presentModal(BudgetItemDetailComponent, {budget: this.budget, item: it});
    this.items = [];
    this.pageNumber = 1;
    this.listContent();
  }

  sortListing(): void {
    this.items = [];
    this.pageNumber = 1;
    this.listContent();
  }

}
