import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {BudgetsPage} from './budgets.page';
import {AuthService} from '../services/auth.service';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthService],
    component: BudgetsPage
  },
  {
    path: 'detail/:budgetId',
    canActivate: [AuthService],
    loadChildren: () => import('./budget-detail/budget-detail.module').then( m => m.BudgetDetailPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BudgetsPageRoutingModule {}
