import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ExchangeRate} from '../../entity/exchange-rate';
import {ExchangeRateService} from '../../services/exchange-rate.service';
import {ErrorMappingService} from '../../services/error-mapping.service';
import {ModalController} from '@ionic/angular';
import {HttpErrorResponse} from '@angular/common/http';
import {Currency} from '../../entity/currency';
import {CurrencySearchComponent} from '../currency-search/currency-search.component';
import {environment} from '../../../environments/environment';
import {AuthService} from '../../services/auth.service';
import {User} from '../../entity/user';
import {SettingService} from '../../services/setting.service';
import {ModalService} from '../../services/modal.service';
import {ActionSheetService} from '../../services/action-sheet.service';
import {DateService} from '../../services/date.service';

@Component({
  selector: 'app-exchange-rate-detail',
  templateUrl: './exchange-rate-detail.component.html',
  styleUrls: ['./exchange-rate-detail.component.scss'],
})
export class ExchangeRateDetailComponent implements OnInit {

  public dateFormat: string = environment.defaultDateFormat;

  form: FormGroup;

  @Input() exchangeRate: ExchangeRate;

  selectedFrom: Currency;

  selectedTo: Currency;

  selectedMultiplier: number;

  multiplierHelper = false;

  editing: boolean;

  isSending = false;

  onForeground = false;

  constructor(
    private exchangeRateService: ExchangeRateService,
    private authService: AuthService,
    private errorMappingService: ErrorMappingService,
    private settingService: SettingService,
    private modalService: ModalService,
    private actionSheetService: ActionSheetService,
    private modalController: ModalController,
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {
    this.editing = this.exchangeRate == null;
    this.selectedFrom = this.exchangeRate != null ? this.exchangeRate.from : null;
    this.selectedTo = this.exchangeRate != null ? this.exchangeRate.to : null;
    this.onForeground = true;
    this.authService.getLoggedIn().then((user: User) => {
      this.dateFormat = this.settingService.getDateFormat(user.dateFormat).selector;
    });
    this.initMultiplier();
    this.initForm();
    if (this.exchangeRate == null) {
      this.startEdit();
    }
  }

  private initMultiplier() {
    this.selectedMultiplier = this.exchangeRate != null ? this.exchangeRate.multiplier : 0;
  }

  multiplierChange(val: number) {
    this.selectedMultiplier = val;
  }

  switchMultiplier() {
    this.multiplierHelper = !this.multiplierHelper;
    this.selectedMultiplier = 1 / this.selectedMultiplier;
    if (this.selectedMultiplier === -Infinity || this.selectedMultiplier === Infinity) {
      this.selectedMultiplier = null;
    }
    this.form.get('multiplier').setValue(this.selectedMultiplier);
  }

  private initForm() {
    this.form = this.formBuilder.group({
      id: new FormControl(this.exchangeRate != null ? this.exchangeRate.id : ''),
      registered: new FormControl(this.exchangeRate != null ? this.exchangeRate.registered : new Date().toISOString(), [
        Validators.required
      ]),
      from: new FormControl(this.selectedFrom != null ? this.selectedFrom.name : '', [
        Validators.required
      ]),
      to: new FormControl(this.selectedTo != null ? this.selectedTo.name : '', [
        Validators.required
      ]),
      multiplier: new FormControl(this.exchangeRate != null ? this.exchangeRate.multiplier : '', [
        Validators.required,
        Validators.pattern('^\\d+(.+\\d)?$')
      ]),
    });
    this.form.get('id').disable();
    this.form.get('registered').disable();
    this.form.get('multiplier').disable();
    this.form.get('from').disable();
    this.form.get('to').disable();
  }

  startEdit() {
    this.editing = true;
    this.form.get('registered').enable();
    this.form.get('registered').markAsTouched();
    this.form.get('from').enable();
    this.form.get('from').markAsTouched();
    this.form.get('to').enable();
    this.form.get('to').markAsTouched();
    this.form.get('multiplier').enable();
    this.form.get('multiplier').markAsTouched();
    this.form.get('from').enable();
    this.form.get('from').markAsTouched();
    this.form.get('to').enable();
    this.form.get('to').markAsTouched();
  }

  save() {
    if (this.form.invalid || !this.editing || this.isSending || !this.onForeground) {
      return false;
    }
    const toSend: ExchangeRate = {
      id: this.exchangeRate != null ? this.exchangeRate.id : null,
      registered: DateService.truncateMillis(this.form.get('registered').value),
      multiplier: this.form.get('multiplier').value
    };
    if (this.multiplierHelper) {
      toSend.multiplier = 1 / toSend.multiplier;
    }
    this.isSending = true;
    this.editing = false;
    if (toSend.id != null && toSend.id > 0) {
      this.exchangeRateService.edit(toSend.id, toSend, this.selectedFrom.id, this.selectedTo.id).subscribe(
        (er: ExchangeRate) => {
          this.exchangeRate = er;
          this.closeModal();
        }, (error: HttpErrorResponse) => {
          this.isSending = false;
          this.editing = true;
          this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
        }
      );
    } else {
      this.exchangeRateService.create(toSend, this.selectedFrom.id, this.selectedTo.id).subscribe(
        (er: ExchangeRate) => {
          this.exchangeRate = er;
          this.closeModal();
        }, (error: HttpErrorResponse) => {
          this.isSending = false;
          this.editing = true;
          this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
        }
      );
    }
  }

  async delete() {
    if (this.exchangeRate != null && this.exchangeRate.id > 0) {
      const handler = () => {
        this.isSending = true;
        this.exchangeRateService.delete(this.exchangeRate.id).subscribe(
          () => {
            this.exchangeRate = null;
            this.closeModal();
          }, (error: HttpErrorResponse) => {
            this.isSending = false;
            this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
          }
        );
      };
      await this.actionSheetService.presentDelete('exchangeRate.delete', null, 'general.delete', 'general.cancel', handler);
    }
  }

  cancelEdit() {
    if (this.exchangeRate == null) {
      this.closeModal();
      return;
    }
    this.editing = false;
    this.form.get('id').disable();
    this.form.get('registered').setValue(this.exchangeRate.registered != null ? this.exchangeRate.registered : '');
    this.form.get('registered').disable();
    this.form.get('multiplier').setValue(this.exchangeRate.multiplier != null ? this.exchangeRate.multiplier : '');
    this.form.get('multiplier').disable();
    this.form.get('from').setValue(this.selectedFrom != null ? this.selectedFrom.name : '');
    this.form.get('from').disable();
    this.form.get('to').setValue(this.selectedTo != null ? this.selectedTo.name : '');
    this.form.get('to').disable();
  }

  public closeModal() {
    this.modalController.dismiss(this.exchangeRate);
  }

  async searchFrom() {
    this.selectedFrom = await this.searchCurrency();
    this.form.get('from').reset(this.selectedFrom != null ? this.selectedFrom.name : '');
    if (this.selectedFrom != null && this.selectedTo != null && this.selectedFrom.id === this.selectedTo.id) {
      this.selectedTo = null;
      this.form.get('to').setValue('');
      this.form.get('to').markAsTouched();
    }
  }

  async searchTo() {
    this.selectedTo = await this.searchCurrency();
    this.form.get('to').reset(this.selectedTo != null ? this.selectedTo.name : '');
    if (this.selectedFrom != null && this.selectedTo != null && this.selectedFrom.id === this.selectedTo.id) {
      this.selectedFrom = null;
      this.form.get('from').setValue('');
      this.form.get('from').markAsTouched();
    }
  }

  private async searchCurrency(): Promise<Currency> {
    this.onForeground = false;
    const ccy: Currency = await this.modalService.presentModal(CurrencySearchComponent);
    this.onForeground = true;
    return ccy;
  }

}
