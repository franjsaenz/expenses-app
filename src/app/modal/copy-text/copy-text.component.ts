import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {ModalController} from '@ionic/angular';

@Component({
  selector: 'app-copy-text',
  templateUrl: './copy-text.component.html',
  styleUrls: ['./copy-text.component.scss'],
})
export class CopyTextComponent implements OnInit {

  copied = false;

  @Input() toCopy: string;

  @ViewChild('copyText') textarea: HTMLIonTextareaElement;

  constructor(
    private modalController: ModalController
  ) {
  }

  ngOnInit() {
  }

  close() {
    this.modalController.dismiss().then();
  }

  async copy() {
    const el: HTMLTextAreaElement = await this.textarea.getInputElement();
    el.select();
    document.execCommand('copy');
    this.copied = true;
  }

}
