import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Budget} from '../../entity/budget';
import {BudgetItem} from '../../entity/budget-item';
import {Category} from '../../entity/category';
import {BudgetService} from '../../services/budget.service';
import {ErrorMappingService} from '../../services/error-mapping.service';
import {ModalController} from '@ionic/angular';
import {HttpErrorResponse} from '@angular/common/http';
import {CategorySearchComponent} from '../category-search/category-search.component';
import {ModalService} from '../../services/modal.service';
import {ActionSheetService} from '../../services/action-sheet.service';

@Component({
  selector: 'app-budget-item-detail',
  templateUrl: './budget-item-detail.component.html',
  styleUrls: ['./budget-item-detail.component.scss'],
})
export class BudgetItemDetailComponent implements OnInit {

  form: FormGroup;

  @Input() budget: Budget;

  @Input() item: BudgetItem;

  selectedCategory: Category;

  editing: boolean;

  isSending = false;

  onForeground = false;

  constructor(
    private budgetService: BudgetService,
    private errorMappingService: ErrorMappingService,
    private modalService: ModalService,
    private actionSheetService: ActionSheetService,
    private modalController: ModalController,
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {
    this.editing = this.item == null;
    this.onForeground = true;
    if (this.item != null) {
      this.selectedCategory = this.item.category;
    }
    this.initForm();
    if (this.item == null) {
      this.startEdit();
    }
  }

  private initForm() {
    this.form = this.formBuilder.group({
      id: new FormControl(this.item != null ? this.item.id : ''),
      category: new FormControl(this.selectedCategory != null ? this.selectedCategory.name : '', [
        Validators.required
      ]),
      amount: new FormControl(this.item != null ? this.item.amount : '', [
        Validators.required,
        Validators.pattern('^\\d+(.+\\d)?$')
      ])
    });
    this.form.get('id').disable();
    this.form.get('category').disable();
    this.form.get('amount').disable();
  }

  startEdit() {
    this.editing = true;
    this.form.get('category').enable();
    this.form.get('category').markAsTouched();
    this.form.get('amount').enable();
    this.form.get('amount').markAsTouched();
  }

  save() {
    if (this.form.invalid || !this.editing || this.isSending || !this.onForeground) {
      return false;
    }
    const toSend: BudgetItem = {
      id: this.item != null ? this.item.id : null,
      amount: this.form.get('amount').value
    };
    this.isSending = true;
    this.editing = false;
    this.budgetService.setItem(this.budget, this.selectedCategory, toSend).subscribe(
      (it: BudgetItem) => {
        this.item = it;
        this.isSending = false;
        this.cancelEdit();
      }, (error: HttpErrorResponse) => {
        this.isSending = false;
        this.editing = true;
        this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
      }
    );
  }

  async delete() {
    if (this.item != null && this.item.id > 0) {
      const handler = () => {
        this.isSending = true;
        this.budgetService.deleteItem(this.budget, this.item).subscribe(
          () => {
            this.item = null;
            this.closeModal();
          }, (error: HttpErrorResponse) => {
            this.isSending = false;
            this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
          }
        );
      };
      await this.actionSheetService.presentDelete('budget.deleteItem', this.item.category.name, 'general.delete', 'general.cancel', handler);
    }
  }

  cancelEdit() {
    if (this.item == null) {
      this.closeModal();
      return;
    }
    this.selectedCategory = this.item.category;
    this.form.get('id').reset(this.item.id);
    this.form.get('id').disable();
    this.form.get('category').reset(this.selectedCategory.name != null ? this.selectedCategory.name : '');
    this.form.get('category').disable();
    this.form.get('amount').reset(this.item.amount);
    this.form.get('amount').disable();
    this.editing = false;
  }

  public closeModal() {
    this.modalController.dismiss(this.item).then();
  }

  async searchCategory() {
    this.onForeground = false;
    const response = await this.modalService.presentModal(CategorySearchComponent);
    this.onForeground = true;
    if (response != null) {
      this.selectedCategory = response;
      this.form.get('category').reset(this.selectedCategory.name != null ? this.selectedCategory.name : '');
    }
  }

}
