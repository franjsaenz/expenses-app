import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ErrorMappingService} from '../../services/error-mapping.service';
import {ModalController} from '@ionic/angular';
import {HttpErrorResponse} from '@angular/common/http';
import {Business} from '../../entity/business';
import {BusinessService} from '../../services/business.service';
import {ActionSheetService} from '../../services/action-sheet.service';

@Component({
  selector: 'app-business-detail',
  templateUrl: './business-detail.component.html',
  styleUrls: ['./business-detail.component.scss'],
})
export class BusinessDetailComponent implements OnInit {

  form: FormGroup;

  @Input() business: Business;

  editing: boolean;

  isSending = false;

  constructor(
    private businessService: BusinessService,
    private errorMappingService: ErrorMappingService,
    private actionSheetService: ActionSheetService,
    private modalController: ModalController,
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {
    this.editing = this.business == null;
    this.initForm();
    if (this.business == null) {
      this.startEdit();
    }
  }

  private initForm() {
    this.form = this.formBuilder.group({
      id: new FormControl(this.business != null ? this.business.id : ''),
      name: new FormControl(this.business != null ? this.business.name : '', [
        Validators.required
      ])
    });
    this.form.get('id').disable();
    this.form.get('name').disable();
  }

  startEdit() {
    this.editing = true;
    this.form.get('name').enable();
    this.form.get('name').markAsTouched();
  }

  save() {
    if (this.form.invalid || !this.editing || this.isSending) {
      return false;
    }
    const toSend: Business = {
      id: this.business != null ? this.business.id : null,
      name: this.form.get('name').value
    };
    this.isSending = true;
    this.editing = false;
    if (toSend.id != null && toSend.id > 0) {
      this.businessService.edit(toSend.id, toSend).subscribe(
        (bus: Business) => {
          this.business = bus;
          this.closeModal();
        }, (error: HttpErrorResponse) => {
          this.isSending = false;
          this.editing = true;
          this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
        }
      );
    } else {
      this.businessService.create(toSend).subscribe(
        (bus: Business) => {
          this.business = bus;
          this.closeModal();
        }, (error: HttpErrorResponse) => {
          this.isSending = false;
          this.editing = true;
          this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
        }
      );
    }
  }

  async delete() {
    if (this.business != null && this.business.id > 0) {
      const handler = () => {
        this.isSending = true;
        this.businessService.delete(this.business.id).subscribe(
          () => {
            this.business = null;
            this.closeModal();
          }, (error: HttpErrorResponse) => {
            this.isSending = false;
            this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
          }
        );
      };
      await this.actionSheetService.presentDelete('business.delete', this.business.name, 'general.delete', 'general.cancel', handler);
    }
  }

  cancelEdit() {
    if (this.business == null) {
      this.closeModal();
      return;
    }
    this.form.get('id').disable();
    this.form.get('name').reset(this.business.name != null ? this.business.name : '');
    this.form.get('name').disable();
    this.editing = false;
  }

  public closeModal() {
    this.modalController.dismiss(this.business).then();
  }

}
