import {Component, Input, OnInit} from '@angular/core';
import {ErrorMappingService} from '../../services/error-mapping.service';
import {ModalController} from '@ionic/angular';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ProductService} from '../../services/product.service';
import {Product} from '../../entity/product';
import {Category} from '../../entity/category';
import {HttpErrorResponse} from '@angular/common/http';
import {CategorySearchComponent} from '../category-search/category-search.component';
import {ModalService} from '../../services/modal.service';
import {ActionSheetService} from '../../services/action-sheet.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss'],
})
export class ProductDetailComponent implements OnInit {

  form: FormGroup;

  @Input() product: Product;

  selectedCategory: Category;

  editing: boolean;

  isSending = false;

  onForeground = false;

  constructor(
    private productService: ProductService,
    private errorMappingService: ErrorMappingService,
    private modalService: ModalService,
    private actionSheetService: ActionSheetService,
    private modalController: ModalController,
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {
    this.editing = this.product == null;
    this.selectedCategory = this.product != null ? this.product.category : null;
    this.onForeground = true;
    this.initForm();
    if (this.product == null) {
      this.startEdit();
    }
  }

  private initForm() {
    this.form = this.formBuilder.group({
      id: new FormControl(this.product != null ? this.product.id : ''),
      name: new FormControl(this.product != null ? this.product.name : '', [
        Validators.required
      ]),
      category: new FormControl(this.selectedCategory != null ? this.selectedCategory.name : '', [
        Validators.required
      ])
    });
    this.form.get('id').disable();
    this.form.get('name').disable();
    this.form.get('category').disable();
  }

  startEdit() {
    this.editing = true;
    this.form.get('name').enable();
    this.form.get('name').markAsTouched();
    this.form.get('category').enable();
    this.form.get('category').markAsTouched();
  }

  save() {
    if (this.form.invalid || !this.editing || this.isSending || !this.onForeground) {
      return false;
    }
    const toSend: Product = {
      id: this.product != null ? this.product.id : null,
      name: this.form.get('name').value
    };
    this.isSending = true;
    this.editing = false;
    if (toSend.id != null && toSend.id > 0) {
      this.productService.edit(toSend.id, toSend, this.selectedCategory).subscribe(
        (prod: Product) => {
          this.product = prod;
          this.closeModal();
        }, (error: HttpErrorResponse) => {
          this.isSending = false;
          this.editing = true;
          this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
        }
      );
    } else {
      this.productService.create(toSend, this.selectedCategory).subscribe(
        (prod: Product) => {
          this.product = prod;
          this.closeModal();
        }, (error: HttpErrorResponse) => {
          this.isSending = false;
          this.editing = true;
          this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
        }
      );
    }
  }

  async delete() {
    if (this.product != null && this.product.id > 0) {
      const handler = () => {
        this.isSending = true;
        this.productService.delete(this.product.id).subscribe(
          () => {
            this.product = null;
            this.closeModal();
          }, (error: HttpErrorResponse) => {
            this.isSending = false;
            this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
          }
        );
      };
      await this.actionSheetService.presentDelete('product.delete', this.product.name, 'general.delete', 'general.cancel', handler);
    }
  }

  cancelEdit() {
    if (this.product == null) {
      this.closeModal();
      return;
    }
    this.editing = false;
    this.selectedCategory = this.product.category;
    this.form.get('id').disable();
    this.form.get('name').reset(this.product.name != null ? this.product.name : '');
    this.form.get('name').disable();
    this.form.get('category').reset(this.selectedCategory.name != null ? this.selectedCategory.name : '');
    this.form.get('category').disable();
  }

  public closeModal() {
    this.modalController.dismiss(this.product);
  }

  async searchCategory() {
    this.onForeground = false;
    const response = await this.modalService.presentModal(CategorySearchComponent);
    this.onForeground = true;
    if (response != null) {
      this.selectedCategory = response;
      this.form.get('category').reset(this.selectedCategory.name != null ? this.selectedCategory.name : '');
    }
  }

}
