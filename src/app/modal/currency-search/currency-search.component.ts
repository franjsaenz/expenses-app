import {Component, OnInit, ViewChild} from '@angular/core';
import {Currency} from '../../entity/currency';
import {IonInfiniteScroll, IonSearchbar, ModalController} from '@ionic/angular';
import {CurrencyService} from '../../services/currency.service';
import {ErrorMappingService} from '../../services/error-mapping.service';
import {Page} from '../../entity/page';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-currency-search',
  templateUrl: './currency-search.component.html',
  styleUrls: ['./currency-search.component.scss'],
})
export class CurrencySearchComponent implements OnInit {

  public loadingContent = true;

  public searchTerm: string;

  public pageNumber = 1;

  public currencies: Array<Currency> = [];

  public selectedItem = -1;

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  @ViewChild(IonSearchbar) searchBar: IonSearchbar;

  constructor(
    private currencyService: CurrencyService,
    private errorMappingService: ErrorMappingService,
    private modalController: ModalController
  ) {
  }

  ngOnInit() {
    this.listContent();
    setTimeout(() => {
      if (this.searchBar != null) {
        this.searchBar.setFocus().then();
      }
    }, 500);
  }

  listContent() {
    this.loadingContent = true;
    if (this.infiniteScroll != null) {
      this.infiniteScroll.disabled = true;
    }
    this.currencyService.search(
      this.searchTerm, this.pageNumber - 1,
      'name',
      true
    ).subscribe((resp: Page<Currency>) => {
      this.loadingContent = false;
      this.currencies = this.currencies.concat(resp.content);
      this.pageNumber++;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = resp.last;
      this.selectedItem = resp.content.length > 0 ? 0 : -1;
    }, (err: HttpErrorResponse) => {
      this.selectedItem = -1;
      this.loadingContent = false;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = true;
      this.errorMappingService.displayError(err.error, ErrorMappingService.WARNING);
    });
  }

  search(term: string) {
    this.resetList();
    if (term == null || term.length < 1) {
      this.searchTerm = null;
    } else {
      this.searchTerm = term;
    }
    this.listContent();
  }

  resetList() {
    this.searchTerm = null;
    this.currencies = [];
    this.pageNumber = 1;
  }

  public closeModal(ccy: Currency) {
    this.modalController.dismiss(ccy);
  }

  closeModalWithSelected() {
    if (this.currencies != null && this.selectedItem >= 0 && this.selectedItem < this.currencies.length) {
      this.closeModal(this.currencies[this.selectedItem]);
    }
  }

  selectNext() {
    if (this.currencies.length > (this.selectedItem + 1)) {
      this.selectedItem += 1;
    }
  }

  selectPrev() {
    if (this.selectedItem > -1) {
      this.selectedItem -= 1;
    }
  }

}
