import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Currency} from '../../entity/currency';
import {IonInfiniteScroll, ModalController} from '@ionic/angular';
import {ErrorMappingService} from '../../services/error-mapping.service';
import {Page} from '../../entity/page';
import {HttpErrorResponse} from '@angular/common/http';
import {ExchangeRate} from '../../entity/exchange-rate';
import {ExchangeRateService} from '../../services/exchange-rate.service';
import {DateFormat, SettingService} from '../../services/setting.service';
import {environment} from '../../../environments/environment';
import {User} from '../../entity/user';
import {AuthService} from '../../services/auth.service';
import {ModalService} from '../../services/modal.service';
import {ExchangeRateDetailComponent} from '../exchange-rate-detail/exchange-rate-detail.component';

@Component({
  selector: 'app-exchange-rate-search',
  templateUrl: './exchange-rate-search.component.html',
  styleUrls: ['./exchange-rate-search.component.scss'],
})
export class ExchangeRateSearchComponent implements OnInit {

  public dateFormat: DateFormat = {pipe: environment.defaultDateFormat, selector: null};

  public loadingContent = true;

  @Input() currency: Currency;

  public pageNumber = 1;

  public rates: Array<ExchangeRate> = [];

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  constructor(
    private exchangeRateService: ExchangeRateService,
    private authService: AuthService,
    private errorMappingService: ErrorMappingService,
    private settingService: SettingService,
    private modalService: ModalService,
    private modalController: ModalController
  ) {
  }

  ngOnInit() {
    this.authService.getLoggedIn().then((user: User) => {
      this.dateFormat = this.settingService.getDateFormat(user.dateFormat);
      this.listContent();
    });
  }

  listContent() {
    this.loadingContent = true;
    if (this.infiniteScroll != null) {
      this.infiniteScroll.disabled = true;
    }
    this.exchangeRateService.search(
      this.currency, this.pageNumber - 1,
      'registered',
      false
    ).subscribe((resp: Page<ExchangeRate>) => {
      this.loadingContent = false;
      this.rates = this.rates.concat(resp.content);
      this.pageNumber++;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = resp.last;
    }, (err: HttpErrorResponse) => {
      this.loadingContent = false;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = true;
      this.errorMappingService.displayError(err.error, ErrorMappingService.WARNING);
    });
  }

  resetList() {
    this.rates = [];
    this.pageNumber = 1;
  }

  async createExchangeRate() {
    const er: ExchangeRate = await this.modalService.presentModal(ExchangeRateDetailComponent);
    if (er != null) {
      setTimeout(() => {
        this.closeModal(er);
      }, 500);
    }
  }

  public closeModal(rate: ExchangeRate) {
    this.modalController.dismiss(rate);
  }

}
