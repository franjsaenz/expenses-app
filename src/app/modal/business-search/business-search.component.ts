import {Component, OnInit, ViewChild} from '@angular/core';
import {Business} from '../../entity/business';
import {IonInfiniteScroll, IonSearchbar, ModalController} from '@ionic/angular';
import {BusinessService} from '../../services/business.service';
import {ErrorMappingService} from '../../services/error-mapping.service';
import {Page} from '../../entity/page';
import {HttpErrorResponse} from '@angular/common/http';
import {ModalService} from '../../services/modal.service';
import {BusinessDetailComponent} from '../business-detail/business-detail.component';

@Component({
  selector: 'app-business-search',
  templateUrl: './business-search.component.html',
  styleUrls: ['./business-search.component.scss'],
})
export class BusinessSearchComponent implements OnInit {

  public loadingContent = true;

  public searchTerm: string;

  public pageNumber = 1;

  public businesses: Array<Business> = [];

  public selectedItem = -1;

  onForeground = false;

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  @ViewChild(IonSearchbar) searchBar: IonSearchbar;

  constructor(
    private businessService: BusinessService,
    private errorMappingService: ErrorMappingService,
    private modalService: ModalService,
    private modalController: ModalController
  ) {
  }

  ngOnInit() {
    this.onForeground = true;
    this.listContent();
    setTimeout(() => {
      if (this.searchBar != null) {
        this.searchBar.setFocus().then();
      }
    }, 500);
  }

  listContent() {
    this.loadingContent = true;
    if (this.infiniteScroll != null) {
      this.infiniteScroll.disabled = true;
    }
    this.businessService.search(
      this.searchTerm, this.pageNumber - 1,
      'name',
      true
    ).subscribe((resp: Page<Business>) => {
      this.loadingContent = false;
      this.businesses = this.businesses.concat(resp.content);
      this.pageNumber++;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = resp.last;
      this.selectedItem = resp.content.length > 0 ? 0 : -1;
    }, (err: HttpErrorResponse) => {
      this.selectedItem = -1;
      this.loadingContent = false;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = true;
      this.errorMappingService.displayError(err.error, ErrorMappingService.WARNING);
    });
  }

  search(term: string) {
    this.resetList();
    if (term == null || term.length < 1) {
      this.searchTerm = null;
    } else {
      this.searchTerm = term;
    }
    this.listContent();
  }

  resetList() {
    this.searchTerm = null;
    this.businesses = [];
    this.pageNumber = 1;
  }

  async createBusiness() {
    this.onForeground = false;
    const business: Business = await this.modalService.presentModal(BusinessDetailComponent);
    this.onForeground = true;
    if (business != null) {
      setTimeout(() => {
        this.closeModal(business);
      }, 500);
    }
  }

  public closeModal(bus: Business) {
    this.modalController.dismiss(bus);
  }

  closeModalWithSelected() {
    if (this.onForeground && this.businesses != null && this.selectedItem >= 0 && this.selectedItem < this.businesses.length) {
      this.closeModal(this.businesses[this.selectedItem]);
    }
  }

  selectNext() {
    if (this.onForeground && this.businesses.length > (this.selectedItem + 1)) {
      this.selectedItem += 1;
    }
  }

  selectPrev() {
    if (this.onForeground && this.selectedItem > -1) {
      this.selectedItem -= 1;
    }
  }
}
