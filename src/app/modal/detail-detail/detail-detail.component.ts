import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Expense} from '../../entity/expense';
import {Detail} from '../../entity/detail';
import {Product} from '../../entity/product';
import {Category} from '../../entity/category';
import {ExpenseService} from '../../services/expense.service';
import {ErrorMappingService} from '../../services/error-mapping.service';
import {IonInput, ModalController} from '@ionic/angular';
import {HttpErrorResponse} from '@angular/common/http';
import {CategorySearchComponent} from '../category-search/category-search.component';
import {ProductSearchComponent} from '../product-search/product-search.component';
import {Response} from '../../entity/response';
import {ModalService} from '../../services/modal.service';
import {ActionSheetService} from '../../services/action-sheet.service';

@Component({
  selector: 'app-detail-detail',
  templateUrl: './detail-detail.component.html',
  styleUrls: ['./detail-detail.component.scss'],
})
export class DetailDetailComponent implements OnInit {

  form: FormGroup;

  @Input() expense: Expense;

  @Input() detail: Detail;

  @ViewChild('amount') amountInput: IonInput;

  selectedProduct: Product;

  selectedCategory: Category;

  overrideCategory = false;

  editing: boolean;

  isSending = false;

  onForeground = false;

  constructor(
    private expenseService: ExpenseService,
    private errorMappingService: ErrorMappingService,
    private modalService: ModalService,
    private actionSheetService: ActionSheetService,
    private modalController: ModalController,
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {
    this.onForeground = true;
    this.editing = this.detail == null;
    if (this.detail != null) {
      this.selectedProduct = this.detail.product;
      this.selectedCategory = this.detail.category;
    }
    this.initForm();
    if (this.detail == null) {
      this.startEdit();
      this.startWizard();
    }
  }

  private startWizard() {
    this.searchProduct().then();
  }

  private initForm() {
    this.form = this.formBuilder.group({
      id: new FormControl(this.detail != null ? this.detail.id : ''),
      product: new FormControl(this.selectedProduct != null ? this.selectedProduct.name : '', [
        Validators.min(1),
        Validators.required
      ]),
      category: new FormControl(this.selectedCategory != null ? this.selectedCategory.name : ''),
      description: new FormControl(this.detail != null ? this.detail.description : ''),
      amount: new FormControl(this.detail != null ? this.detail.amount : '', [
        Validators.required,
        Validators.pattern('^\\d+(.+\\d)?$')
      ]),
      repetitions: new FormControl(1, [
        Validators.pattern('^\\d*$'),
          Validators.max(100)
      ]),
      withoutDiscount: new FormControl(this.detail != null ? this.detail.withoutDiscount : false)
    });
    this.form.get('id').disable();
    this.form.get('product').disable();
    this.form.get('category').disable();
    this.form.get('description').disable();
    this.form.get('amount').disable();
    this.form.get('repetitions').disable();
    this.form.get('withoutDiscount').disable();
  }

  startEdit(focusOnAmount: boolean = false) {
    if (focusOnAmount) {
      setTimeout(() => {
        this.amountInput.setFocus();
      }, 500);
    }
    this.editing = true;
    this.form.get('product').enable();
    this.form.get('product').markAsTouched();
    this.form.get('category').enable();
    this.form.get('category').markAsTouched();
    this.form.get('description').enable();
    this.form.get('description').markAsTouched();
    this.form.get('amount').enable();
    this.form.get('amount').markAsTouched();
    this.form.get('repetitions').enable();
    this.form.get('repetitions').markAsTouched();
    this.form.get('withoutDiscount').enable();
    this.form.get('withoutDiscount').markAsTouched();
  }

  save(thenClose: boolean = true) {
    if (this.form.invalid || !this.editing || this.isSending || !this.onForeground) {
      return false;
    }
    let desc: string = this.form.get('description').value;
    if (desc == null || desc.length < 1) {
      desc = null;
    }
    const toSend: Detail = {
      id: this.detail != null ? this.detail.id : null,
      amount: this.form.get('amount').value,
      description: desc,
      withoutDiscount: this.form.get('withoutDiscount').value
    };
    this.isSending = true;
    this.editing = false;
    if (toSend.id != null && toSend.id > 0) {
      this.expenseService.editDetail(this.expense.id, toSend, this.selectedProduct, this.selectedCategory).subscribe(
        (det: Detail) => {
          this.isSending = false;
          this.detail = det;
          this.cancelEdit();
        }, (error: HttpErrorResponse) => {
          this.isSending = false;
          this.editing = true;
          this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
        }
      );
    } else {
      const repetitions: number = this.form.get('repetitions').value;
      this.expenseService.createDetail(this.expense.id, toSend, this.selectedProduct, this.selectedCategory, repetitions).subscribe(
        (ignored: Response) => {
          this.detail = null;
          if (thenClose) {
            this.closeModal();
          } else {
            this.isSending = false;
            this.resetForm();
            this.startEdit();
            this.startWizard();
          }
        }, (error: HttpErrorResponse) => {
          this.isSending = false;
          this.editing = true;
          this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
        }
      );
    }
  }

  private resetForm() {
    this.selectedProduct = null;
    this.selectedCategory = null;
    this.overrideCategory = false;
    this.form.get('product').reset('');
    this.form.get('product').disable();
    this.form.get('category').reset('');
    this.form.get('category').disable();
    this.form.get('amount').reset('');
    this.form.get('amount').disable();
    this.form.get('description').reset('');
    this.form.get('description').disable();
    this.form.get('repetitions').reset(1);
    this.form.get('repetitions').disable();
    this.form.get('withoutDiscount').reset(false);
    this.form.get('withoutDiscount').disable();
    this.editing = false;
  }

  async delete() {
    if (this.detail != null && this.detail.id > 0) {
      const handler = () => {
        this.isSending = true;
        this.expenseService.deleteDetail(this.expense.id, this.detail).subscribe(
          () => {
            this.detail = null;
            this.closeModal();
          }, (error: HttpErrorResponse) => {
            this.isSending = false;
            this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
          }
        );
      };
      await this.actionSheetService.presentDelete(
        'expense.deleteDetail', this.detail.product.name,
        'general.delete', 'general.cancel', handler
      );
    }
  }

  cancelEdit() {
    if (this.detail == null) {
      this.closeModal();
      return;
    }
    this.selectedProduct = this.detail.product;
    this.selectedCategory = this.detail.category;
    this.form.get('product').reset(this.selectedProduct.name != null ? this.selectedProduct.name : '');
    this.form.get('product').disable();
    this.form.get('category').reset(this.selectedCategory.name != null ? this.selectedCategory.name : '');
    this.form.get('category').disable();
    this.form.get('amount').reset(this.detail.amount);
    this.form.get('amount').disable();
    this.form.get('description').reset(this.detail.description != null ? this.detail.description : '');
    this.form.get('description').disable();
    this.form.get('repetitions').reset(1);
    this.form.get('repetitions').disable();
    this.form.get('withoutDiscount').reset(this.detail.withoutDiscount != null ? this.detail.withoutDiscount : false);
    this.form.get('withoutDiscount').disable();
    this.editing = false;
  }

  public closeModal() {
    this.modalController.dismiss(this.detail).then();
  }

  async searchCategory() {
    this.onForeground = false;
    const response = await this.modalService.presentModal(CategorySearchComponent);
    this.onForeground = true;
    setTimeout(() => {
      this.amountInput.setFocus();
    }, 500);
    if (response != null) {
      this.selectedCategory = response;
      this.form.get('category').reset(this.selectedCategory.name != null ? this.selectedCategory.name : '');
      this.overrideCategory = true;
    }
  }

  async searchProduct() {
    this.onForeground = false;
    const response = await this.modalService.presentModal(ProductSearchComponent);
    this.onForeground = true;
    setTimeout(() => {
      this.amountInput.setFocus();
    }, 500);
    if (response != null) {
      this.selectedProduct = response;
      this.form.get('product').reset(this.selectedProduct.name != null ? this.selectedProduct.name : '');
      if (!this.overrideCategory) {
        this.selectedCategory = this.selectedProduct.category;
        this.form.get('category').reset(this.selectedCategory.name != null ? this.selectedCategory.name : '');
      }
    }
  }

  resetCategory() {
    this.overrideCategory = false;
    this.selectedCategory = this.selectedProduct != null ? this.selectedProduct.category : null;
    this.form.get('category').reset(this.selectedCategory != null ? this.selectedCategory.name : '');
  }

}
