import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Budget} from '../../entity/budget';
import {BudgetService} from '../../services/budget.service';
import {ErrorMappingService} from '../../services/error-mapping.service';
import {Currency} from '../../entity/currency';
import {HttpErrorResponse} from '@angular/common/http';
import {CurrencySearchComponent} from '../currency-search/currency-search.component';
import {User} from '../../entity/user';
import {AuthService} from '../../services/auth.service';
import {SettingService} from '../../services/setting.service';
import {ModalService} from '../../services/modal.service';
import {ModalController} from '@ionic/angular';
import {DateService} from '../../services/date.service';

@Component({
  selector: 'app-budget-create',
  templateUrl: './budget-create.component.html',
  styleUrls: ['./budget-create.component.scss'],
})
export class BudgetCreateComponent implements OnInit {

  dateFormat: string;

  readonly maxDate: string = new Date(new Date().getFullYear() + 2, 1, 1).toISOString();

  form: FormGroup;

  @Input() toCopy: Budget;

  selectedCurrency: Currency;

  isSending = false;

  constructor(
    private budgetService: BudgetService,
    private authService: AuthService,
    private settingService: SettingService,
    private errorMappingService: ErrorMappingService,
    private modalService: ModalService,
    private modalController: ModalController,
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {
    if (this.toCopy != null) {
      this.selectedCurrency = this.toCopy.currency;
    }
    this.authService.getLoggedIn().then((user: User) => {
      this.dateFormat = this.settingService.getDateFormat(user.dateFormat).selector;
      this.initForm();
    });
    this.settingService.getSetting(SettingService.defaultCurrencyKey).then(
      (currency: Currency) => {
        if (currency != null && currency.id > 0) {
          this.setCurrency(currency);
        }
      }
    );
  }

  private initForm() {
    this.form = this.formBuilder.group({
      name: new FormControl('', [
        Validators.required
      ]),
      from: new FormControl(this.toCopy != null ? this.toCopy.from : '', [
        Validators.required
      ]),
      to: new FormControl(this.toCopy != null ? this.toCopy.to : '', [
        Validators.required
      ]),
      currency: new FormControl(this.selectedCurrency != null ? this.selectedCurrency.name : '', [
        Validators.required
      ])
    });
    this.form.get('name').enable();
    this.form.get('name').markAsTouched();
    this.form.get('from').enable();
    this.form.get('from').markAsTouched();
    this.form.get('to').enable();
    this.form.get('to').markAsTouched();
    this.form.get('currency').enable();
    this.form.get('currency').markAsTouched();
  }

  save() {
    const toSend: Budget = {
      name: this.form.get('name').value,
      from: DateService.truncateMillis(this.form.get('from').value),
      to: DateService.truncateMillis(this.form.get('to').value),
    };
    this.isSending = true;
    this.budgetService.create(toSend, this.selectedCurrency, this.toCopy).subscribe(
      (bdg: Budget) => {
        this.isSending = false;
        this.close(bdg);
      }, (error: HttpErrorResponse) => {
        this.isSending = false;
        this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
      }
    );
  }

  async searchCurrency() {
    const response = await this.modalService.presentModal(CurrencySearchComponent);
    if (response != null) {
      this.setCurrency(response);
    }
  }

  private setCurrency(currency: Currency) {
    this.selectedCurrency = currency;
    if (this.form != null) {
      this.form.get('currency').setValue(this.selectedCurrency.name);
    }
  }

  close(bdg: Budget = null) {
    this.modalController.dismiss(bdg).then();
  }

}
