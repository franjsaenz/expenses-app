import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Currency} from '../../entity/currency';
import {CurrencyService} from '../../services/currency.service';
import {ErrorMappingService} from '../../services/error-mapping.service';
import {ModalController} from '@ionic/angular';
import {HttpErrorResponse} from '@angular/common/http';
import {ActionSheetService} from '../../services/action-sheet.service';

@Component({
  selector: 'app-currency-detail',
  templateUrl: './currency-detail.component.html',
  styleUrls: ['./currency-detail.component.scss'],
})
export class CurrencyDetailComponent implements OnInit {

  form: FormGroup;

  @Input() currency: Currency;

  editing: boolean;

  isSending = false;

  constructor(
    private currencyService: CurrencyService,
    private errorMappingService: ErrorMappingService,
    private actionSheetService: ActionSheetService,
    private modalController: ModalController,
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {
    this.editing = this.currency == null;
    this.initForm();
    if (this.currency == null) {
      this.startEdit();
    }
  }

  private initForm() {
    this.form = this.formBuilder.group({
      id: new FormControl(this.currency != null ? this.currency.id : ''),
      name: new FormControl(this.currency != null ? this.currency.name : '', [
        Validators.required
      ]),
      identifier: new FormControl(this.currency != null ? this.currency.identifier : '', [
        Validators.required
      ]),
      code: new FormControl(this.currency != null ? this.currency.code : '', [
        Validators.required
      ])
    });
    this.form.get('id').disable();
    this.form.get('name').disable();
    this.form.get('identifier').disable();
    this.form.get('code').disable();
  }

  startEdit() {
    this.editing = true;
    this.form.get('name').enable();
    this.form.get('name').markAsTouched();
    this.form.get('identifier').enable();
    this.form.get('identifier').markAsTouched();
    this.form.get('code').enable();
    this.form.get('code').markAsTouched();
  }

  save() {
    if (this.form.invalid || !this.editing || this.isSending) {
      return false;
    }
    const toSend: Currency = {
      id: this.currency != null ? this.currency.id : null,
      name: this.form.get('name').value,
      identifier: this.form.get('identifier').value,
      code: this.form.get('code').value,
    };
    this.isSending = true;
    this.editing = false;
    if (toSend.id != null && toSend.id > 0) {
      this.currencyService.edit(toSend.id, toSend).subscribe(
        (ccy: Currency) => {
          this.currency = ccy;
          this.closeModal();
        }, (error: HttpErrorResponse) => {
          this.isSending = false;
          this.editing = true;
          this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
        }
      );
    } else {
      this.currencyService.create(toSend).subscribe(
        (ccy: Currency) => {
          toSend.id = ccy.id;
          this.currency = ccy;
          this.closeModal();
        }, (error: HttpErrorResponse) => {
          this.isSending = false;
          this.editing = true;
          this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
        }
      );
    }
  }

  async delete() {
    if (this.currency != null && this.currency.id > 0) {
      const handler = () => {
        this.isSending = true;
        this.currencyService.delete(this.currency.id).subscribe(
          () => {
            this.currency = null;
            this.closeModal();
          }, (error: HttpErrorResponse) => {
            this.isSending = false;
            this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
          }
        );
      };
      await this.actionSheetService.presentDelete('currency.delete', this.currency.name, 'general.delete', 'general.cancel', handler);
    }
  }

  cancelEdit() {
    if (this.currency == null) {
      this.closeModal();
      return;
    }
    this.editing = false;
    this.form.get('id').disable();
    this.form.get('name').reset(this.currency.name != null ? this.currency.name : '');
    this.form.get('name').disable();
    this.form.get('identifier').reset(this.currency.identifier != null ? this.currency.identifier : '');
    this.form.get('identifier').disable();
    this.form.get('code').reset(this.currency.code != null ? this.currency.code : '');
    this.form.get('code').disable();
  }

  public closeModal() {
    this.modalController.dismiss(this.currency);
  }

}
