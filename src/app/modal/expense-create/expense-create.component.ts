import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Expense} from '../../entity/expense';
import {Currency} from '../../entity/currency';
import {Business} from '../../entity/business';
import {ExpenseService} from '../../services/expense.service';
import {AuthService} from '../../services/auth.service';
import {DateFormat, SettingService} from '../../services/setting.service';
import {ErrorMappingService} from '../../services/error-mapping.service';
import {ModalController} from '@ionic/angular';
import {User} from '../../entity/user';
import {HttpErrorResponse} from '@angular/common/http';
import {CurrencySearchComponent} from '../currency-search/currency-search.component';
import {BusinessSearchComponent} from '../business-search/business-search.component';
import {environment} from '../../../environments/environment';
import {ExchangeRate} from '../../entity/exchange-rate';
import {ExchangeRateSearchComponent} from '../exchange-rate-search/exchange-rate-search.component';
import {ModalService} from '../../services/modal.service';
import {DateService} from '../../services/date.service';

@Component({
  selector: 'app-expense-create',
  templateUrl: './expense-create.component.html',
  styleUrls: ['./expense-create.component.scss'],
})
export class ExpenseCreateComponent implements OnInit {

  dateFormat: string;

  pipeDateFormat: string = environment.defaultDateFormat;

  form: FormGroup;

  @Input() toCopy: Expense;

  selectedCurrency: Currency;

  selectedBusiness: Business;

  selectedRate: ExchangeRate;

  isSending = false;

  onForeground = false;

  constructor(
    private expenseService: ExpenseService,
    private authService: AuthService,
    private settingService: SettingService,
    private errorMappingService: ErrorMappingService,
    private modalService: ModalService,
    private modalController: ModalController,
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {
    this.onForeground = true;
    if (this.toCopy != null) {
      this.selectedCurrency = this.toCopy.currency;
      this.selectedBusiness = this.toCopy.business;
      this.selectedRate = this.toCopy.exchangeRate;
    }
    this.authService.getLoggedIn().then((user: User) => {
      const df: DateFormat = this.settingService.getDateFormat(user.dateFormat);
      this.dateFormat = df.selector;
      this.pipeDateFormat = df.pipe;
      this.initForm();
    });
    this.settingService.getSetting(SettingService.defaultCurrencyKey).then(
      (currency: Currency) => {
        if (currency != null && currency.id > 0) {
          this.setCurrency(currency);
        }
      }
    );
  }

  private initForm() {
    this.form = this.formBuilder.group({
      registered: new FormControl('', [
        Validators.required
      ]),
      business: new FormControl(this.selectedBusiness != null ? this.selectedBusiness.name : '', [
        Validators.required
      ]),
      currency: new FormControl(this.selectedCurrency != null ? this.selectedCurrency.name : '', [
        Validators.required
      ]),
      rate: new FormControl(this.calculateRate())
    });
    this.form.get('registered').enable();
    this.form.get('registered').markAsTouched();
    this.form.get('business').enable();
    this.form.get('business').markAsTouched();
    this.form.get('currency').enable();
    this.form.get('currency').markAsTouched();
    this.form.get('rate').enable();
  }

  private calculateRate(): string {
    if (this.selectedRate == null) {
      return '';
    }
    let toRate: number = this.selectedRate.multiplier;
    let fromRate: number;
    const fromCcy: string = this.selectedRate.from.code;
    const toCcy: string = this.selectedRate.to.code;
    if (this.selectedCurrency.id !== this.selectedRate.from.id) {
      fromRate = 1.00;
    } else {
      fromRate = 1 / toRate;
      toRate = 1.00;
    }
    return fromRate.toFixed(2) + ' ' + fromCcy + ' = ' + toRate.toFixed(2) + ' ' + toCcy;
  }

  save() {
    if (!this.form || this.form.invalid || this.isSending || !this.onForeground) {
      return false;
    }
    const toSend: Expense = {
      registered: DateService.truncateMillis(this.form.get('registered').value)
    };
    this.isSending = true;
    this.expenseService.create(toSend, this.selectedBusiness, this.selectedCurrency, this.selectedRate, this.toCopy).subscribe(
      (exp: Expense) => {
        this.isSending = false;
        this.close(exp);
      }, (error: HttpErrorResponse) => {
        this.isSending = false;
        this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
      }
    );
  }

  async searchCurrency() {
    this.onForeground = false;
    const response = await this.modalService.presentModal(CurrencySearchComponent);
    this.onForeground = true;
    if (response != null) {
      this.setCurrency(response);
    }
  }

  private setCurrency(currency: Currency) {
    this.selectedCurrency = currency;
    if (this.form != null) {
      this.form.get('currency').setValue(this.selectedCurrency.name);
    }
  }

  async searchBusiness() {
    this.onForeground = false;
    const response = await this.modalService.presentModal(BusinessSearchComponent);
    this.onForeground = true;
    if (response != null) {
      this.selectedBusiness = response;
      this.form.get('business').setValue(this.selectedBusiness.name);
    }
  }

  async searchExchangeRate() {
    this.onForeground = false;
    this.selectedRate = await this.modalService.presentModal(ExchangeRateSearchComponent, {currency: this.selectedCurrency});
    this.onForeground = true;
    if (this.selectedRate == null && this.toCopy != null) {
      this.selectedRate = this.toCopy.exchangeRate;
    }
    this.form.get('rate').reset(this.calculateRate());
  }

  async clearExchangeRate() {
    this.selectedRate = null;
    this.form.get('rate').reset(this.calculateRate());
  }

  close(exp: Expense = null) {
    this.modalController.dismiss(exp).then();
  }

}
