import {Component, OnInit, ViewChild} from '@angular/core';
import {IonInfiniteScroll, IonSearchbar, ModalController} from '@ionic/angular';
import {Budget} from '../../entity/budget';
import {BudgetService} from '../../services/budget.service';
import {ErrorMappingService} from '../../services/error-mapping.service';
import {Page} from '../../entity/page';
import {HttpErrorResponse} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {User} from '../../entity/user';
import {AuthService} from '../../services/auth.service';
import {SettingService} from '../../services/setting.service';

@Component({
  selector: 'app-budget-search',
  templateUrl: './budget-search.component.html',
  styleUrls: ['./budget-search.component.scss'],
})
export class BudgetSearchComponent implements OnInit {

  public dateFormat: string = environment.defaultDateFormat;

  public loadingContent = true;

  public searchTerm: string;

  public pageNumber = 1;

  public budgets: Array<Budget> = [];

  public selectedItem = -1;

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  @ViewChild(IonSearchbar) searchBar: IonSearchbar;

  constructor(
    private budgetService: BudgetService,
    private authService: AuthService,
    private settingService: SettingService,
    private errorMappingService: ErrorMappingService,
    private modalController: ModalController
  ) {
  }

  ngOnInit() {
    this.authService.getLoggedIn().then((user: User) => {
      this.dateFormat = this.settingService.getDateFormat(user.dateFormat).pipe;
      this.listContent();
      setTimeout(() => {
        if (this.searchBar != null) {
          this.searchBar.setFocus().then();
        }
      }, 500);
    });
  }

  listContent() {
    this.loadingContent = true;
    if (this.infiniteScroll != null) {
      this.infiniteScroll.disabled = true;
    }
    this.budgetService.search(
      this.searchTerm, null,
      this.pageNumber - 1,
      'to', false
    ).subscribe((resp: Page<Budget>) => {
      this.loadingContent = false;
      this.budgets = this.budgets.concat(resp.content);
      this.pageNumber++;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = resp.last;
      this.selectedItem = resp.content.length > 0 ? 0 : -1;
    }, (err: HttpErrorResponse) => {
      this.selectedItem = -1;
      this.loadingContent = false;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = true;
      this.errorMappingService.displayError(err.error, ErrorMappingService.WARNING);
    });
  }

  search(term: string) {
    this.resetList();
    if (term == null || term.length < 1) {
      this.searchTerm = null;
    } else {
      this.searchTerm = term;
    }
    this.listContent();
  }

  resetList() {
    this.searchTerm = null;
    this.budgets = [];
    this.pageNumber = 1;
  }

  public closeModal(bdg: Budget) {
    this.modalController.dismiss(bdg);
  }

  closeModalWithSelected() {
    if (this.budgets != null && this.selectedItem >= 0 && this.selectedItem < this.budgets.length) {
      this.closeModal(this.budgets[this.selectedItem]);
    }
  }

  selectNext() {
    if (this.budgets.length > (this.selectedItem + 1)) {
      this.selectedItem += 1;
    }
  }

  selectPrev() {
    if (this.selectedItem > -1) {
      this.selectedItem -= 1;
    }
  }
}
