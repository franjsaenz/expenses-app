import {Component, OnInit, ViewChild} from '@angular/core';
import {CategoryService} from '../../services/category.service';
import {ErrorMappingService} from '../../services/error-mapping.service';
import {IonInfiniteScroll, IonSearchbar, ModalController} from '@ionic/angular';
import {Category} from '../../entity/category';
import {Page} from '../../entity/page';
import {HttpErrorResponse} from '@angular/common/http';
import {ModalService} from '../../services/modal.service';
import {CategoryDetailComponent} from '../category-detail/category-detail.component';

@Component({
  selector: 'app-category-search',
  templateUrl: './category-search.component.html',
  styleUrls: ['./category-search.component.scss'],
})
export class CategorySearchComponent implements OnInit {

  public loadingContent = true;

  public searchTerm: string;

  public pageNumber = 1;

  public categories: Array<Category> = [];

  public selectedItem = -1;

  onForeground = false;

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  @ViewChild(IonSearchbar) searchBar: IonSearchbar;

  constructor(
    private categoriesService: CategoryService,
    private errorMappingService: ErrorMappingService,
    private modalService: ModalService,
    private modalController: ModalController
  ) {
  }

  ngOnInit() {
    this.onForeground = true;
    this.listContent();
    setTimeout(() => {
      if (this.searchBar != null) {
        this.searchBar.setFocus().then();
      }
    }, 500);
  }

  listContent() {
    this.loadingContent = true;
    if (this.infiniteScroll != null) {
      this.infiniteScroll.disabled = true;
    }
    this.categoriesService.search(
      this.searchTerm, this.pageNumber - 1,
      'name',
      true
    ).subscribe((resp: Page<Category>) => {
      this.loadingContent = false;
      this.categories = this.categories.concat(resp.content);
      this.pageNumber++;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = resp.last;
      this.selectedItem = resp.content.length > 0 ? 0 : -1;
    }, (err: HttpErrorResponse) => {
      this.selectedItem = -1;
      this.loadingContent = false;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = true;
      this.errorMappingService.displayError(err.error, ErrorMappingService.WARNING);
    });
  }

  search(term: string) {
    this.resetList();
    if (term == null || term.length < 1) {
      this.searchTerm = null;
    } else {
      this.searchTerm = term;
    }
    this.listContent();
  }

  resetList() {
    this.searchTerm = null;
    this.categories = [];
    this.pageNumber = 1;
  }

  async createCategory() {
    this.onForeground = false;
    const cat: Category = await this.modalService.presentModal(CategoryDetailComponent);
    this.onForeground = true;
    if (cat != null) {
      setTimeout(() => {
        this.closeModal(cat);
      }, 500);
    }
  }

  public closeModal(cat: Category) {
    this.modalController.dismiss(cat);
  }

  closeModalWithSelected() {
    if (this.onForeground && this.categories != null && this.selectedItem >= 0 && this.selectedItem < this.categories.length) {
      this.closeModal(this.categories[this.selectedItem]);
    }
  }

  selectNext() {
    if (this.onForeground && this.categories.length > (this.selectedItem + 1)) {
      this.selectedItem += 1;
    }
  }

  selectPrev() {
    if (this.onForeground && this.selectedItem > -1) {
      this.selectedItem -= 1;
    }
  }
}
