import { Component, Input, OnInit, ViewChild } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Discount} from '../../entity/discount';
import {ExpenseService} from '../../services/expense.service';
import {ErrorMappingService} from '../../services/error-mapping.service';
import { IonInput, ModalController } from '@ionic/angular';
import {HttpErrorResponse} from '@angular/common/http';
import {Expense} from '../../entity/expense';
import {ActionSheetService} from '../../services/action-sheet.service';

@Component({
  selector: 'app-discount-detail',
  templateUrl: './discount-detail.component.html',
  styleUrls: ['./discount-detail.component.scss'],
})
export class DiscountDetailComponent implements OnInit {

  form: FormGroup;

  @Input() expense: Expense;

  @Input() discount: Discount;

  @ViewChild('amount') amountInput: IonInput;

  editing: boolean;

  isSending = false;

  constructor(
    private expenseService: ExpenseService,
    private errorMappingService: ErrorMappingService,
    private actionSheetService: ActionSheetService,
    private modalController: ModalController,
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {
    this.editing = this.discount == null;
    this.initForm();
    if (this.discount == null) {
      this.startEdit();
    }
  }

  private initForm() {
    this.form = this.formBuilder.group({
      id: new FormControl(this.discount != null ? this.discount.id : ''),
      description: new FormControl(this.discount != null ? this.discount.description : ''),
      amount: new FormControl(this.discount != null ? this.discount.amount : '', [
        Validators.required,
        Validators.pattern('^\\d+(.+\\d)?$')
      ]),
    });
    this.form.get('id').disable();
    this.form.get('description').disable();
    this.form.get('amount').disable();
  }

  startEdit() {
    this.editing = true;
    this.form.get('description').enable();
    this.form.get('description').markAsTouched();
    this.form.get('amount').enable();
    this.form.get('amount').markAsTouched();
    setTimeout(() => {
      this.amountInput.setFocus();
    }, 500);
  }

  save() {
    if (this.form.invalid || !this.editing || this.isSending) {
      return false;
    }
    let desc: string = this.form.get('description').value;
    if (desc == null || desc.length < 1) {
      desc = null;
    }
    const toSend: Discount = {
      id: this.discount != null ? this.discount.id : null,
      amount: this.form.get('amount').value,
      description: desc
    };
    this.isSending = true;
    this.editing = false;
    if (toSend.id != null && toSend.id > 0) {
      this.expenseService.editDiscount(this.expense.id, toSend).subscribe(
        (disc: Discount) => {
          this.discount = disc;
          this.closeModal();
        }, (error: HttpErrorResponse) => {
          this.isSending = false;
          this.editing = true;
          this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
        }
      );
    } else {
      this.expenseService.createDiscount(this.expense.id, toSend).subscribe(
        (disc: Discount) => {
          this.discount = disc;
          this.closeModal();
        }, (error: HttpErrorResponse) => {
          this.isSending = false;
          this.editing = true;
          this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
        }
      );
    }
  }

  async delete() {
    if (this.discount != null && this.discount.id > 0) {
      const handler = () => {
        this.isSending = true;
        this.expenseService.deleteDiscount(this.expense.id, this.discount).subscribe(
          () => {
            this.discount = null;
            this.closeModal();
          }, (error: HttpErrorResponse) => {
            this.isSending = false;
            this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
          }
        );
      };
      await this.actionSheetService.presentDelete('expense.deleteDiscount', null, 'general.delete', 'general.cancel', handler);
    }
  }

  cancelEdit() {
    if (this.discount == null) {
      this.closeModal();
      return;
    }
    this.form.get('id').disable();
    this.form.get('amount').reset(this.discount.amount);
    this.form.get('amount').disable();
    this.form.get('description').reset(this.discount.description != null ? this.discount.description : '');
    this.form.get('description').disable();
    this.editing = false;
  }

  public closeModal() {
    this.modalController.dismiss(this.discount).then();
  }

}
