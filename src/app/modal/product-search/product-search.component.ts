import {Component, OnInit, ViewChild} from '@angular/core';
import {IonInfiniteScroll, IonSearchbar, ModalController} from '@ionic/angular';
import {Product} from '../../entity/product';
import {ErrorMappingService} from '../../services/error-mapping.service';
import {ProductService} from '../../services/product.service';
import {Page} from '../../entity/page';
import {HttpErrorResponse} from '@angular/common/http';
import {ModalService} from '../../services/modal.service';
import {ProductDetailComponent} from '../product-detail/product-detail.component';

@Component({
  selector: 'app-product-search',
  templateUrl: './product-search.component.html',
  styleUrls: ['./product-search.component.scss'],
})
export class ProductSearchComponent implements OnInit {

  public loadingContent = true;

  public searchTerm: string;

  public pageNumber = 1;

  public products: Array<Product> = [];
  public selectedItem = -1;

  onForeground = false;

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  @ViewChild(IonSearchbar) searchBar: IonSearchbar;

  constructor(
    private productService: ProductService,
    private errorMappingService: ErrorMappingService,
    private modalService: ModalService,
    private modalController: ModalController
  ) {
  }

  ngOnInit() {
    this.onForeground = true;
    this.listContent();
    setTimeout(() => {
      if (this.searchBar != null) {
        this.searchBar.setFocus().then();
      }
    }, 500);
  }

  listContent() {
    this.loadingContent = true;
    if (this.infiniteScroll != null) {
      this.infiniteScroll.disabled = true;
    }
    this.productService.search(
      this.searchTerm,
      null,
      this.pageNumber - 1,
      'name',
      true
    ).subscribe((resp: Page<Product>) => {
      this.loadingContent = false;
      this.products = this.products.concat(resp.content);
      this.pageNumber++;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = resp.last;
      this.selectedItem = resp.content.length > 0 ? 0 : -1;
    }, (err: HttpErrorResponse) => {
      this.selectedItem = -1;
      this.loadingContent = false;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = true;
      this.errorMappingService.displayError(err.error, ErrorMappingService.WARNING);
    });
  }

  search(term: string) {
    this.resetList();
    if (term == null || term.length < 1) {
      this.searchTerm = null;
    } else {
      this.searchTerm = term;
    }
    this.listContent();
  }

  resetList() {
    this.searchTerm = null;
    this.products = [];
    this.pageNumber = 1;
  }

  async createProduct() {
    this.onForeground = false;
    const prod: Product = await this.modalService.presentModal(ProductDetailComponent);
    this.onForeground = true;
    if (prod != null) {
      setTimeout(() => {
        this.closeModal(prod);
      }, 500);
    }
  }

  async closeModal(prod: Product) {
    return this.modalController.dismiss(prod);
  }

  closeModalWithSelected() {
    if (this.onForeground && this.products != null && this.selectedItem >= 0 && this.selectedItem < this.products.length) {
      this.closeModal(this.products[this.selectedItem]);
    }
  }

  selectNext() {
    if (this.onForeground && this.products.length > (this.selectedItem + 1)) {
      this.selectedItem += 1;
    }
  }

  selectPrev() {
    if (this.onForeground && this.selectedItem > -1) {
      this.selectedItem -= 1;
    }
  }

}
