import {Component, Input, OnInit} from '@angular/core';
import {Category} from '../../entity/category';
import {ModalController} from '@ionic/angular';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CategoryService} from '../../services/category.service';
import {HttpErrorResponse} from '@angular/common/http';
import {ErrorMappingService} from '../../services/error-mapping.service';
import {ActionSheetService} from '../../services/action-sheet.service';

@Component({
  selector: 'app-category-detail',
  templateUrl: './category-detail.component.html',
  styleUrls: ['./category-detail.component.scss'],
})
export class CategoryDetailComponent implements OnInit {

  form: FormGroup;

  @Input() category: Category;

  editing: boolean;

  isSending = false;

  constructor(
    private categoryService: CategoryService,
    private errorMappingService: ErrorMappingService,
    private actionSheetService: ActionSheetService,
    private modalController: ModalController,
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {
    this.editing = this.category == null;
    this.initForm();
    if (this.category == null) {
      this.startEdit();
    }
  }

  private initForm() {
    this.form = this.formBuilder.group({
      id: new FormControl(this.category != null ? this.category.id : ''),
      name: new FormControl(this.category != null ? this.category.name : '', [
        Validators.required
      ]),
      description: new FormControl(this.category != null ? this.category.description : '')
    });
    this.form.get('id').disable();
    this.form.get('name').disable();
    this.form.get('description').disable();
  }

  startEdit() {
    this.editing = true;
    this.form.get('name').enable();
    this.form.get('name').markAsTouched();
    this.form.get('description').enable();
  }

  save() {
    if (this.form.invalid || !this.editing || this.isSending) {
      return false;
    }
    let desc: string = this.form.get('description').value;
    if (desc != null && desc.length < 1) {
      desc = null;
    }
    const toSend: Category = {
      id: this.category != null ? this.category.id : null,
      name: this.form.get('name').value,
      description: desc,
      created: null,
      updated: null,
      deleted: null
    };
    this.isSending = true;
    this.editing = false;
    if (toSend.id != null && toSend.id > 0) {
      this.categoryService.edit(toSend.id, toSend).subscribe(
        (cat: Category) => {
          this.category = cat;
          this.closeModal();
        }, (error: HttpErrorResponse) => {
          this.isSending = false;
          this.editing = true;
          this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
        }
      );
    } else {
      this.categoryService.create(toSend).subscribe(
        (cat: Category) => {
          this.category = cat;
          this.closeModal();
        }, (error: HttpErrorResponse) => {
          this.isSending = false;
          this.editing = true;
          this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
        }
      );
    }
  }

  async delete() {
    if (this.category != null && this.category.id > 0) {
      const handler = () => {
        this.isSending = true;
        this.categoryService.delete(this.category.id).subscribe(
          () => {
            this.category = null;
            this.closeModal();
          }, (error: HttpErrorResponse) => {
            this.isSending = false;
            this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
          }
        );
      };
      await this.actionSheetService.presentDelete('category.delete', this.category.name, 'general.delete', 'general.cancel', handler);
    }
  }

  cancelEdit() {
    if (this.category == null) {
      this.closeModal();
      return;
    }
    this.editing = false;
    this.form.get('id').disable();
    this.form.get('name').reset(this.category.name != null ? this.category.name : '');
    this.form.get('name').disable();
    this.form.get('description').reset(this.category.description != null ? this.category.description : '');
    this.form.get('description').disable();
  }

  public closeModal() {
    this.modalController.dismiss(this.category);
  }

}
