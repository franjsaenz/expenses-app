import {Component, OnInit, ViewChild} from '@angular/core';
import {Currency} from '../entity/currency';
import {IonInfiniteScroll} from '@ionic/angular';
import {Sort, SortConfig} from '../component/sort-popover/sort-popover.component';
import {CurrencyService} from '../services/currency.service';
import {ErrorMappingService} from '../services/error-mapping.service';
import {Page} from '../entity/page';
import {HttpErrorResponse} from '@angular/common/http';
import {CurrencyDetailComponent} from '../modal/currency-detail/currency-detail.component';
import {Subscription} from 'rxjs';
import {ModalService} from '../services/modal.service';
import {PopoverService} from '../services/popover.service';
import {LanguageService} from '../services/language.service';

@Component({
  selector: 'app-currencies',
  templateUrl: './currencies.page.html',
  styleUrls: ['./currencies.page.scss'],
})
export class CurrenciesPage implements OnInit {

  public loadingContent = true;

  public searchTerm: string;

  public pageNumber = 1;

  public currencies: Array<Currency> = [];

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  public sortConfig: SortConfig;

  constructor(
    private currencyService: CurrencyService,
    private errorMappingService: ErrorMappingService,
    private modalService: ModalService,
    private popoverService: PopoverService,
    private languageService: LanguageService
  ) {
    languageService.setTitle('currency.plural');
  }

  ngOnInit() {
    this.sortConfig = this.currencyService.getCurrencySortingConfig();
    this.listContent();
  }

  listContent() {
    this.loadingContent = true;
    if (this.infiniteScroll != null) {
      this.infiniteScroll.disabled = true;
    }
    this.currencyService.search(
      this.searchTerm, this.pageNumber - 1,
      this.sortConfig.currentSort.property,
      this.sortConfig.currentSort.ascending
    ).subscribe((resp: Page<Currency>) => {
      this.loadingContent = false;
      this.currencies = this.currencies.concat(resp.content);
      this.pageNumber++;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = resp.last;
    }, (err: HttpErrorResponse) => {
      this.loadingContent = false;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = true;
      this.errorMappingService.displayError(err.error, ErrorMappingService.WARNING);
    });
  }

  search(term: string) {
    this.resetList();
    if (term == null || term.length < 1) {
      this.searchTerm = null;
    } else {
      this.searchTerm = term;
    }
    this.listContent();
  }

  resetList() {
    this.searchTerm = null;
    this.currencies = [];
    this.pageNumber = 1;
  }

  async openCurrency(ccy: Currency, index: number) {
    const response = await this.modalService.presentModal(CurrencyDetailComponent, {currency: ccy});
    if (response == null || index == null || index < 0) {
      this.currencies = [];
      this.pageNumber = 1;
      this.listContent();
    } else {
      this.currencies[index] = response;
    }
  }

  async presentSort(ev: any) {
    const sortSub: Subscription = this.currencyService.subscribeToSort((sort: Sort) => {
      this.sortConfig.currentSort = sort;
      this.sortListing();
    });
    await this.popoverService.presentSort(ev, this.sortConfig, sortSub);
  }

  sortListing(): void {
    this.currencies = [];
    this.pageNumber = 1;
    this.listContent();
  }

}
