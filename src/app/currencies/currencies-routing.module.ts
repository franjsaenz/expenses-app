import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {CurrenciesPage} from './currencies.page';
import {AuthService} from '../services/auth.service';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthService],
    component: CurrenciesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CurrenciesPageRoutingModule {}
