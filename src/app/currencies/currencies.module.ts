import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {CurrenciesPageRoutingModule} from './currencies-routing.module';

import {CurrenciesPage} from './currencies.page';
import {FlexLayoutModule} from '@angular/flex-layout';
import {CurrencyDetailComponent} from '../modal/currency-detail/currency-detail.component';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CurrenciesPageRoutingModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    TranslateModule
  ],
  declarations: [CurrenciesPage, CurrencyDetailComponent]
})
export class CurrenciesPageModule {
}
