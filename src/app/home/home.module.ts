import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HomePage} from './home.page';

import {HomePageRoutingModule} from './home-routing.module';
import {BudgetCardComponent} from './budget-card/budget-card.component';
import {BusinessCardComponent} from './business-card/business-card.component';
import {ProductCardComponent} from './product-card/product-card.component';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    ReactiveFormsModule,
    TranslateModule
  ],
  declarations: [
    HomePage,
    BudgetCardComponent,
    ProductCardComponent,
    BusinessCardComponent
  ]
})
export class HomePageModule {
}
