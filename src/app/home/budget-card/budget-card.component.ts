import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {BudgetReport} from '../../entity/budget-report';
import {Chart} from 'chart.js';
import {BudgetService} from '../../services/budget.service';
import {ErrorMappingService} from '../../services/error-mapping.service';
import {ChartService} from '../../services/chart.service';
import {BudgetSearchComponent} from '../../modal/budget-search/budget-search.component';
import {Budget} from '../../entity/budget';
import {HttpErrorResponse} from '@angular/common/http';
import {DateFormat, SettingService} from '../../services/setting.service';
import {HomeSetting} from '../home.page';
import {environment} from '../../../environments/environment';
import {AuthService} from '../../services/auth.service';
import {User} from '../../entity/user';
import {ModalService} from '../../services/modal.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-budget-card',
  templateUrl: './budget-card.component.html',
  styleUrls: ['./budget-card.component.scss'],
})
export class BudgetCardComponent implements OnInit {

  private readonly homeSettingKey = 'HOME_SETTING';

  public dateFormat: DateFormat = {pipe: environment.defaultDateFormat, selector: null};

  report: BudgetReport;

  @ViewChild('budgetGraph') graphCanvas: ElementRef;

  graph: Chart;

  graphAttempts = 0;

  graphLoading = true;

  constructor(
    private budgetService: BudgetService,
    private authService: AuthService,
    private errorMappingService: ErrorMappingService,
    private settingService: SettingService,
    private chartService: ChartService,
    private modalService: ModalService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.authService.getLoggedIn().then((user: User) => {
      this.dateFormat = this.settingService.getDateFormat(user.dateFormat);
    });
    this.settingService.getSetting(this.homeSettingKey).then(
      (homeSetting: HomeSetting) => {
        if (homeSetting != null && homeSetting.budget != null) {
          this.loadBudget(homeSetting.budget);
        } else {
          this.graphLoading = false;
        }
      }
    );
  }

  private loadBudget(budget: Budget) {
    this.budgetService.getExpanded(budget.id).subscribe(
      (report: BudgetReport) => {
        this.report = report;
        this.initGraph();
        this.saveBudget(report.budget);
      },
      (error: HttpErrorResponse) => {
        this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
        this.graphLoading = false;
      }
    );
  }

  private async saveBudget(bdg: Budget) {
    let homeSetting: HomeSetting = await this.settingService.getSetting(this.homeSettingKey);
    if (homeSetting == null) {
      homeSetting = {};
    }
    homeSetting.budget = bdg;
    await this.settingService.storeSetting(this.homeSettingKey, homeSetting);
  }

  async resetBudget() {
    await this.saveBudget(null);
    this.report = null;
    if (this.graphCanvas != null) {
      this.graphCanvas.nativeElement.innerHTML = '';
    }
  }

  openBudget() {
    this.router.navigate(['budgets', 'detail', this.report.budget.id]);
  }

  private initGraph() {
    setTimeout(
      () => {
        if (this.graphCanvas != null && this.report != null) {
          this.chartService.createBudgetChart(this.report, this.graphCanvas).then(graph => {
            this.graph = graph;
          });
          this.graphAttempts = 0;
          this.graphLoading = false;
        } else {
          this.graphAttempts++;
          setTimeout(() => {
            if (this.graphAttempts > 10) {
              console.error('Unable to create Graph: ', {graph: this.graph, canvas: this.graphCanvas, report: this.report});
              this.graphLoading = false;
            } else {
              this.initGraph();
            }
          }, 500);
        }
      }, 200);
  }

  toggleBarLabels() {
    this.graph.options.scales.yAxes[0].display = !this.graph.options.scales.yAxes[0].display;
    this.graph.update();
  }

  async searchBudget() {
    const response = await this.modalService.presentModal(BudgetSearchComponent);
    if (response != null) {
      this.loadBudget(response);
    }
  }

}
