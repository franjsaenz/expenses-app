import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {DateFormat, SettingService} from '../../services/setting.service';
import {environment} from '../../../environments/environment';
import {Currency} from '../../entity/currency';
import {Business} from '../../entity/business';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {StatService} from '../../services/stat.service';
import {AuthService} from '../../services/auth.service';
import {ErrorMappingService} from '../../services/error-mapping.service';
import {ChartService} from '../../services/chart.service';
import {User} from '../../entity/user';
import {Group} from '../../entity/group';
import {CurrencySearchComponent} from '../../modal/currency-search/currency-search.component';
import {BusinessSearchComponent} from '../../modal/business-search/business-search.component';
import {HttpErrorResponse} from '@angular/common/http';
import {Chart} from 'chart.js';
import {ModalService} from '../../services/modal.service';
import {DateService} from '../../services/date.service';

@Component({
  selector: 'app-business-card',
  templateUrl: './business-card.component.html',
  styleUrls: ['./business-card.component.scss'],
})
export class BusinessCardComponent implements OnInit {

  public dateFormat: DateFormat = {pipe: environment.defaultDateFormat, selector: null};

  selectedCurrency: Currency;

  selectedBusiness: Business;

  stats: Group;

  @ViewChild('businessGraph') graphCanvas: ElementRef;

  graph: Chart;

  graphAttempts = 0;

  form: FormGroup;

  constructor(
    private statService: StatService,
    private authService: AuthService,
    private errorMappingService: ErrorMappingService,
    private settingService: SettingService,
    private chartService: ChartService,
    private modalService: ModalService,
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {
    this.authService.getLoggedIn().then((user: User) => {
      this.dateFormat = this.settingService.getDateFormat(user.dateFormat);
      this.initForm();
    });
    this.settingService.getSetting(SettingService.defaultCurrencyKey).then(
      (currency: Currency) => {
        if (currency != null && currency.id > 0) {
          this.setCurrency(currency);
        }
      }
    );
  }

  private initForm() {
    this.form = this.formBuilder.group({
      business: new FormControl('', [
        Validators.required
      ]),
      currency: new FormControl('', [
        Validators.required
      ]),
      from: new FormControl('', [
        Validators.required
      ]),
      to: new FormControl('', [
        Validators.required
      ])
    });
    this.enableForm();
  }

  private enableForm() {
    this.form.get('business').enable();
    this.form.get('business').markAsTouched();
    this.form.get('currency').enable();
    this.form.get('currency').markAsTouched();
    this.form.get('from').enable();
    this.form.get('from').markAsTouched();
    this.form.get('to').enable();
    this.form.get('to').markAsTouched();
  }

  disableForm() {
    this.form.get('business').disable();
    this.form.get('currency').disable();
    this.form.get('from').disable();
    this.form.get('to').disable();
  }

  getStats() {
    const from = DateService.truncateMillis(this.form.get('from').value);
    const to = DateService.truncateMillis(this.form.get('to').value);
    this.statService.byBusiness(this.selectedCurrency, this.selectedBusiness, from, to).subscribe(
      (grp: Group) => {
        this.stats = grp;
        this.disableForm();
        this.initGraph();
      },
      (error: HttpErrorResponse) => {
        this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
      }
    );
  }

  clearStat() {
    this.stats = null;
    if (this.graph != null) {
      this.graph.destroy();
      this.graph = null;
    }
    if (this.graphCanvas != null) {
      this.graphCanvas.nativeElement.innerHTML = '';
    }
    this.enableForm();
  }

  private initGraph() {
    setTimeout(
      () => {
        if (this.graphCanvas != null && this.stats != null) {
          this.graph = this.chartService.createBusinessChart(this.stats, this.graphCanvas);
          this.graphAttempts = 0;
        } else {
          this.graphAttempts++;
          setTimeout(() => {
            if (this.graphAttempts > 10) {
              console.error('Unable to create Graph: ', {graph: this.graph, canvas: this.graphCanvas, stats: this.stats});
            } else {
              this.initGraph();
            }
          }, 500);
        }
      }, 200);
  }

  toggleBarLabels() {
    this.graph.options.scales.yAxes[0].display = !this.graph.options.scales.yAxes[0].display;
    this.graph.update();
  }

  async searchCurrency() {
    const response = await this.modalService.presentModal(CurrencySearchComponent);
    if (response != null) {
      this.setCurrency(response);
    }
  }

  private setCurrency(currency: Currency) {
    this.selectedCurrency = currency;
    if (this.form != null) {
      this.form.get('currency').setValue(this.selectedCurrency.name);
      if (this.form.valid) {
        this.getStats();
      }
    }
  }

  async searchBusiness() {
    const response = await this.modalService.presentModal(BusinessSearchComponent);
    if (response != null) {
      this.selectedBusiness = response;
      this.form.get('business').setValue(this.selectedBusiness.name);
      if (this.form.valid) {
        this.getStats();
      }
    }
  }

}
