import {Component, OnInit} from '@angular/core';
import {Budget} from '../entity/budget';
import {LanguageService} from '../services/language.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(
    private languageService: LanguageService
  ) {
    languageService.setTitle('home.singular');
  }

  ngOnInit(): void {
  }

}

export class HomeSetting {
  budget?: Budget;
}
