import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {ExchangeRatesPage} from './exchange-rates.page';
import {AuthService} from '../services/auth.service';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthService],
    component: ExchangeRatesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExchangeRatesPageRoutingModule {}
