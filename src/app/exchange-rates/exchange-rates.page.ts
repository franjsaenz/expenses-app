import {Component, OnInit, ViewChild} from '@angular/core';
import {ExchangeRate} from '../entity/exchange-rate';
import {IonInfiniteScroll} from '@ionic/angular';
import {Sort, SortConfig} from '../component/sort-popover/sort-popover.component';
import {ExchangeRateService} from '../services/exchange-rate.service';
import {ErrorMappingService} from '../services/error-mapping.service';
import {Page} from '../entity/page';
import {HttpErrorResponse} from '@angular/common/http';
import {ExchangeRateDetailComponent} from '../modal/exchange-rate-detail/exchange-rate-detail.component';
import {Subscription} from 'rxjs';
import {User} from '../entity/user';
import {AuthService} from '../services/auth.service';
import {environment} from '../../environments/environment';
import {SettingService} from '../services/setting.service';
import {ModalService} from '../services/modal.service';
import {PopoverService} from '../services/popover.service';
import {LanguageService} from '../services/language.service';

@Component({
  selector: 'app-exchange-rates',
  templateUrl: './exchange-rates.page.html',
  styleUrls: ['./exchange-rates.page.scss'],
})
export class ExchangeRatesPage implements OnInit {

  public dateFormat: string = environment.defaultDateFormat;

  public loadingContent = true;

  public pageNumber = 1;

  public exchangeRates: Array<ExchangeRate> = [];

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  public sortConfig: SortConfig;

  constructor(
    private exchangeRateService: ExchangeRateService,
    private authService: AuthService,
    private errorMappingService: ErrorMappingService,
    private settingService: SettingService,
    private modalService: ModalService,
    private popoverService: PopoverService,
    private languageService: LanguageService
  ) {
    languageService.setTitle('exchangeRate.plural');
  }

  ngOnInit() {
    this.sortConfig = this.exchangeRateService.getExchangeRateSortingConfig();
    this.authService.getLoggedIn().then((user: User) => {
      this.dateFormat = this.settingService.getDateFormat(user.dateFormat).pipe;
    });
    this.listContent();
  }

  listContent() {
    this.loadingContent = true;
    if (this.infiniteScroll != null) {
      this.infiniteScroll.disabled = true;
    }
    this.exchangeRateService.search(
      null, this.pageNumber - 1,
      this.sortConfig.currentSort.property,
      this.sortConfig.currentSort.ascending
    ).subscribe((resp: Page<ExchangeRate>) => {
      this.loadingContent = false;
      this.exchangeRates = this.exchangeRates.concat(resp.content);
      this.pageNumber++;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = resp.last;
    }, (err: HttpErrorResponse) => {
      this.loadingContent = false;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = true;
      this.errorMappingService.displayError(err.error, ErrorMappingService.WARNING);
    });
  }

  async openExchangeRate(er: ExchangeRate, index: number) {
    const response = await this.modalService.presentModal(ExchangeRateDetailComponent, {exchangeRate: er});
    if (response == null || index == null || index < 0) {
      this.exchangeRates = [];
      this.pageNumber = 1;
      this.listContent();
    } else {
      this.exchangeRates[index] = response;
    }
  }

  async presentSort(ev: any) {
    const sortSub: Subscription = this.exchangeRateService.subscribeToSort((sort: Sort) => {
      this.sortConfig.currentSort = sort;
      this.sortListing();
    });
    await this.popoverService.presentSort(ev, this.sortConfig, sortSub);
  }

  sortListing(): void {
    this.exchangeRates = [];
    this.pageNumber = 1;
    this.listContent();
  }

}
