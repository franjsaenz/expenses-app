import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {ExchangeRatesPageRoutingModule} from './exchange-rates-routing.module';

import {ExchangeRatesPage} from './exchange-rates.page';
import {ExchangeRateDetailComponent} from '../modal/exchange-rate-detail/exchange-rate-detail.component';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExchangeRatesPageRoutingModule,
    ReactiveFormsModule,
    TranslateModule
  ],
  declarations: [
    ExchangeRatesPage,
    ExchangeRateDetailComponent
  ]
})
export class ExchangeRatesPageModule {
}
