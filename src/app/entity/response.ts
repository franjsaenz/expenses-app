export class Response {
  timestamp: string;
  message: string;
}
