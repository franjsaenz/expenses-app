export class GroupDetail {
  id?: number;
  name?: string;
  date?: string;
  subtotal?: number;
  discounts?: number;
  total?: number;
}
