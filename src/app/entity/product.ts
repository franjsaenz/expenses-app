import {Category} from './category';

export class Product {
  id?: number;
  created?: string;
  updated?: string;
  deleted?: string;
  name: string;
  category?: Category;
}
