import {Category} from './category';

export class BudgetItem {
  id?: number;
  created?: string;
  updated?: string;
  deleted?: string;
  category?: Category;
  amount: number;
}
