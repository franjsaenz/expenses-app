export class Status {
  name: string;
  version: string;
  build: string;
  startUp: string;
  message: string;
}
