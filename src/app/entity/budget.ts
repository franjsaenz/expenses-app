import {Currency} from './currency';

export class Budget {
  id?: number;
  created?: string;
  updated?: string;
  deleted?: string;
  currency?: Currency;
  name: string;
  from: string;
  to: string;
}
