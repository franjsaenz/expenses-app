import {Currency} from './currency';
import {Business} from './business';
import {Product} from './product';
import {Category} from './category';
import {GroupDetail} from './group-detail';

export class Group {
  from: string;
  to: string;
  currency: Currency;
  business: Business;
  product: Product;
  category: Category;
  details: Array<GroupDetail>;
  subtotal: number;
  discounts: number;
  total: number;
}
