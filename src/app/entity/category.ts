export class Category {
  id?: number;
  created?: string;
  updated?: string;
  deleted?: string;
  name: string;
  description?: string;
}
