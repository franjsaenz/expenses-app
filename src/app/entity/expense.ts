import {Business} from './business';
import {Currency} from './currency';
import {ExchangeRate} from './exchange-rate';
import {GroupDetail} from './group-detail';

export class Expense {
  id?: number;
  created?: string;
  updated?: string;
  deleted?: string;
  registered: string;
  status?: string;
  business?: Business;
  currency?: Currency;
  exchangeRate?: ExchangeRate;
  totalByCategory?: Array<GroupDetail>;
  subtotal?: number;
  discounts?: number;
  total?: number;
}
