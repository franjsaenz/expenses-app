export class Currency {
  id?: number;
  created?: string;
  updated?: string;
  deleted?: string;
  name: string;
  identifier: string;
  code: string;
}
