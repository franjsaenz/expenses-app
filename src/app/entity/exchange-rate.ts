import {Currency} from './currency';

export class ExchangeRate {
  id?: number;
  created?: string;
  updated?: string;
  deleted?: string;
  registered: string;
  from?: Currency;
  to?: Currency;
  multiplier: number;
}
