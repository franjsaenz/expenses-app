export class Exception {
  timestamp: string;
  status: number;
  error: string;
  message: string;
  path: string;
  meta: object;
  errorCode: string;
}
