export class User {
  id?: number;
  created?: string;
  updated?: string;
  deleted?: string;
  role?: string;
  email?: string;
  password?: string;
  zoneId: string;
  dateFormat: string;
  token?: string;
  expiration?: string;
  notBefore?: string;
}
