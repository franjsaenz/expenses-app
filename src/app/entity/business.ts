export class Business {
  id?: number;
  created?: string;
  updated?: string;
  deleted?: string;
  name: string;
}
