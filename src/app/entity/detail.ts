import {Product} from './product';
import {Category} from './category';

export class Detail {
  id?: number;
  created?: string;
  updated?: string;
  deleted?: string;
  product?: Product;
  category?: Category;
  amount: number;
  discount?: number;
  subtotal?: number;
  description?: string;
  withoutDiscount?: boolean;
}
