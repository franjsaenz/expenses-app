import {GroupDetail} from './group-detail';
import {BudgetReportItem} from './budget-report-item';
import {Budget} from './budget';

export class BudgetReport {
  budget: Budget;
  details: Array<BudgetReportItem>;
  unaccounted: Array<GroupDetail>;
  totalExpected: number;
  totalExpended: number;
  totalRemainedToExpend: number;
  totalUnaccounted: number;
  totalCompleted: number;
}
