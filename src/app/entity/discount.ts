export class Discount {
  id?: number;
  created?: string;
  updated?: string;
  deleted?: string;
  amount: number;
  description?: string;
}
