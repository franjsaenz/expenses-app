export class BudgetReportItem {
  id: number;
  name: string;
  expected: number;
  expended: number;
  remainedToExpend: number;
  completed: number;
}
