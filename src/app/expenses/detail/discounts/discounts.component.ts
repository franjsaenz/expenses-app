import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Expense} from '../../../entity/expense';
import {IonInfiniteScroll} from '@ionic/angular';
import {Sort, SortConfig} from '../../../component/sort-popover/sort-popover.component';
import {Subscription} from 'rxjs';
import {Discount} from '../../../entity/discount';
import {ExpenseService} from '../../../services/expense.service';
import {ErrorMappingService} from '../../../services/error-mapping.service';
import {Page} from '../../../entity/page';
import {HttpErrorResponse} from '@angular/common/http';
import {DiscountDetailComponent} from '../../../modal/discount-detail/discount-detail.component';
import {ModalService} from '../../../services/modal.service';

@Component({
  selector: 'app-discounts',
  templateUrl: './discounts.component.html',
  styleUrls: ['./discounts.component.scss'],
})
export class DiscountsComponent implements OnInit, OnDestroy {

  @Input() expense: Expense;

  public loadingContent = true;

  public pageNumber = 1;

  public discounts: Array<Discount> = [];

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  public sortConfig: SortConfig;

  private sortSub: Subscription;

  constructor(
    private expenseService: ExpenseService,
    private errorMappingService: ErrorMappingService,
    private modalService: ModalService
  ) {
  }

  ngOnInit() {
    this.sortConfig = this.expenseService.getDiscountSortingConfig();
    this.sortSub = this.expenseService.subscribeDiscountSort((sort: Sort) => {
      this.sortConfig.currentSort = sort;
      this.sortListing();
    });
    this.listContent();
  }

  listContent() {
    this.loadingContent = true;
    if (this.infiniteScroll != null) {
      this.infiniteScroll.disabled = true;
    }
    this.expenseService.listDiscounts(
      this.expense.id, this.pageNumber - 1,
      this.sortConfig.currentSort.property,
      this.sortConfig.currentSort.ascending
    ).subscribe((resp: Page<Discount>) => {
      this.loadingContent = false;
      this.discounts = this.discounts.concat(resp.content);
      this.pageNumber++;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = resp.last;
    }, (err: HttpErrorResponse) => {
      this.loadingContent = false;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = true;
      this.errorMappingService.displayError(err.error, ErrorMappingService.WARNING);
    });
  }

  ngOnDestroy() {
    this.sortConfig.publisher.next({property: null, ascending: null});
    this.sortSub.unsubscribe();
  }

  resetList() {
    this.discounts = [];
    this.pageNumber = 1;
  }

  async openDiscount(disc: Discount, index: number) {
    const response = await this.modalService.presentModal(DiscountDetailComponent, {expense: this.expense, discount: disc});
    if (response == null || index == null || index < 0) {
      this.discounts = [];
      this.pageNumber = 1;
      this.listContent();
    } else {
      this.discounts[index] = response;
    }
  }

  sortListing(): void {
    this.discounts = [];
    this.pageNumber = 1;
    this.listContent();
  }

}
