import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {DetailPage} from './detail.page';
import {AuthService} from '../../services/auth.service';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthService],
    component: DetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailPageRoutingModule {}
