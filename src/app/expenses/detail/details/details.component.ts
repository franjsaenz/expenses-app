import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Expense} from '../../../entity/expense';
import {ExpenseService} from '../../../services/expense.service';
import {IonInfiniteScroll} from '@ionic/angular';
import {Sort, SortConfig} from '../../../component/sort-popover/sort-popover.component';
import {Detail} from '../../../entity/detail';
import {ErrorMappingService} from '../../../services/error-mapping.service';
import {Page} from '../../../entity/page';
import {HttpErrorResponse} from '@angular/common/http';
import {Subscription} from 'rxjs';
import {DetailDetailComponent} from '../../../modal/detail-detail/detail-detail.component';
import {ModalService} from '../../../services/modal.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class DetailsComponent implements OnInit, OnDestroy {

  @Input() expense: Expense;

  public loadingContent = true;

  public pageNumber = 1;

  public details: Array<Detail> = [];

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  public sortConfig: SortConfig;

  private sortSub: Subscription;

  constructor(
    private expenseService: ExpenseService,
    private errorMappingService: ErrorMappingService,
    private modalService: ModalService
  ) {
  }

  ngOnInit() {
    this.sortConfig = this.expenseService.getDetailsSortingConfig();
    this.sortSub = this.expenseService.subscribeDetailSort((sort: Sort) => {
      this.sortConfig.currentSort = sort;
      this.sortListing();
    });
    this.listContent();
  }

  ngOnDestroy() {
    this.sortConfig.publisher.next({property: null, ascending: null});
    this.sortSub.unsubscribe();
  }

  listContent() {
    this.loadingContent = true;
    if (this.infiniteScroll != null) {
      this.infiniteScroll.disabled = true;
    }
    this.expenseService.listDetails(
      this.expense.id, this.pageNumber - 1,
      this.sortConfig.currentSort.property,
      this.sortConfig.currentSort.ascending
    ).subscribe((resp: Page<Detail>) => {
      this.loadingContent = false;
      this.details = this.details.concat(resp.content);
      this.pageNumber++;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = resp.last;
    }, (err: HttpErrorResponse) => {
      this.loadingContent = false;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = true;
      this.errorMappingService.displayError(err.error, ErrorMappingService.WARNING);
    });
  }

  resetList() {
    this.details = [];
    this.pageNumber = 1;
  }

  async openDetail(det: Detail, index: number) {
    const response = await this.modalService.presentModal(DetailDetailComponent, {expense: this.expense, detail: det});
    if (response == null || index == null || index < 0) {
      this.details = [];
      this.pageNumber = 1;
      this.listContent();
    } else {
      this.details[index] = response;
    }
  }

  sortListing(): void {
    this.details = [];
    this.pageNumber = 1;
    this.listContent();
  }

}
