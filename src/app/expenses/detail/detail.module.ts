import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {DetailPageRoutingModule} from './detail-routing.module';

import {DetailPage} from './detail.page';
import {DetailsComponent} from './details/details.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {DiscountsComponent} from './discounts/discounts.component';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailPageRoutingModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    TranslateModule
  ],
  declarations: [
    DetailPage,
    DetailsComponent,
    DiscountsComponent
  ]
})
export class DetailPageModule {}
