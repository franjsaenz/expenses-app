import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ExpenseService} from '../../services/expense.service';
import {ErrorMappingService} from '../../services/error-mapping.service';
import {Expense} from '../../entity/expense';
import {HttpErrorResponse} from '@angular/common/http';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {SettingService} from '../../services/setting.service';
import {User} from '../../entity/user';
import {AuthService} from '../../services/auth.service';
import {SortConfig} from '../../component/sort-popover/sort-popover.component';
import {Currency} from '../../entity/currency';
import {CurrencySearchComponent} from '../../modal/currency-search/currency-search.component';
import {Business} from '../../entity/business';
import {BusinessSearchComponent} from '../../modal/business-search/business-search.component';
import {ExpenseCreateComponent} from '../../modal/expense-create/expense-create.component';
import {ExchangeRate} from '../../entity/exchange-rate';
import {ExchangeRateSearchComponent} from '../../modal/exchange-rate-search/exchange-rate-search.component';
import {ModalService} from '../../services/modal.service';
import {PopoverService} from '../../services/popover.service';
import {ActionSheetService} from '../../services/action-sheet.service';
import {Location} from '@angular/common';
import {LanguageService} from '../../services/language.service';
import {DateService} from '../../services/date.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {

  dateFormat: string;

  private id: number;

  editing = false;

  isSending = false;

  expense: Expense;

  selectedCurrency: Currency;

  selectedBusiness: Business;

  selectedRate: ExchangeRate;

  otherCurrency: Currency;

  multiplier: number;

  form: FormGroup;

  segment = 'info';

  detailSortConfig: SortConfig;

  discountSortConfig: SortConfig;

  constructor(
    private expenseService: ExpenseService,
    private authService: AuthService,
    private errorMappingService: ErrorMappingService,
    private settingService: SettingService,
    private modalService: ModalService,
    private popoverService: PopoverService,
    private actionSheetService: ActionSheetService,
    private languageService: LanguageService,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private location: Location
  ) {
    languageService.setTitle('expense.expenseDetail');
    const id: any = activatedRoute.snapshot.params.expenseId;
    if (id != null) {
      try {
        this.id = +id;
      } catch (e) {
        console.error(e);
        this.id = null;
      }
    }
    this.detailSortConfig = expenseService.getDetailsSortingConfig();
    this.discountSortConfig = expenseService.getDiscountSortingConfig();
  }

  ngOnInit() {
    if (this.id == null || isNaN(this.id) || this.id < 1) {
      this.close();
      return;
    }
    this.authService.getLoggedIn().then((user: User) => {
      this.dateFormat = this.settingService.getDateFormat(user.dateFormat).selector;
    });
    this.refreshExpense();
  }

  segmentChanged(newSegment: string) {
    if ('info' === newSegment) {
      this.refreshExpense();
    }
  }

  private refreshExpense() {
    this.isSending = true;
    this.expenseService.get(this.id).subscribe(
      (exp: Expense) => {
        this.expense = exp;
        this.selectedCurrency = exp.currency;
        this.selectedBusiness = exp.business;
        this.selectedRate = exp.exchangeRate;
        this.editing = exp.status === 'OPENED';
        this.isSending = false;
        this.presetOtherCurrency();
        this.initForm();
      },
      (error: HttpErrorResponse) => {
        this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
        this.close();
      }
    );
  }

  private presetOtherCurrency() {
    if (this.expense.exchangeRate != null) {
      if (this.expense.currency.id === this.expense.exchangeRate.from.id) {
        this.otherCurrency = this.expense.exchangeRate.to;
        this.multiplier = this.expense.exchangeRate.multiplier;
      } else {
        this.otherCurrency = this.expense.exchangeRate.from;
        this.multiplier = 1 / this.expense.exchangeRate.multiplier;
      }
    } else {
      this.otherCurrency = null;
      this.multiplier = null;
    }
  }

  private initForm() {
    this.form = this.formBuilder.group({
      id: new FormControl(this.expense.id),
      registered: new FormControl(this.expense.registered, [
        Validators.required
      ]),
      business: new FormControl(this.selectedBusiness.name, [
        Validators.required
      ]),
      currency: new FormControl(this.selectedCurrency.name, [
        Validators.required
      ]),
      rate: new FormControl(this.calculateRate())
    });
    this.form.get('id').disable();
    if (this.editing) {
      this.form.get('registered').enable();
      this.form.get('business').enable();
      this.form.get('currency').enable();
      this.form.get('rate').enable();
    } else {
      this.form.get('registered').disable();
      this.form.get('business').disable();
      this.form.get('currency').disable();
      this.form.get('rate').disable();
    }
  }

  private calculateRate(): string {
    if (this.selectedRate == null) {
      return '';
    }
    let toRate: number = this.selectedRate.multiplier;
    let fromRate: number;
    const fromCcy: string = this.selectedRate.from.code;
    const toCcy: string = this.selectedRate.to.code;
    if (this.selectedCurrency.id !== this.selectedRate.from.id) {
      fromRate = 1.00;
    } else {
      fromRate = 1 / toRate;
      toRate = 1.00;
    }
    return fromRate.toFixed(2) + ' ' + fromCcy + ' = ' + toRate.toFixed(2) + ' ' + toCcy;
  }

  private resetForm() {
    this.selectedBusiness = this.expense.business;
    this.selectedCurrency = this.expense.currency;
    this.selectedRate = this.expense.exchangeRate;
    this.form.get('registered').setValue(this.expense.registered);
    this.form.get('business').setValue(this.selectedBusiness.name);
    this.form.get('currency').setValue(this.selectedCurrency.name);
    this.form.get('rate').setValue(this.calculateRate());
    if (this.editing) {
      this.form.get('registered').enable();
      this.form.get('business').enable();
      this.form.get('currency').enable();
      this.form.get('rate').enable();
    } else {
      this.form.get('registered').disable();
      this.form.get('business').disable();
      this.form.get('currency').disable();
      this.form.get('rate').disable();
    }
    this.presetOtherCurrency();
  }

  changeStatus(newStatus: string) {
    this.isSending = true;
    this.expenseService.updateStatus(this.expense.id, newStatus).subscribe(
      (exp: Expense) => {
        this.expense = exp;
        this.selectedCurrency = exp.currency;
        this.selectedBusiness = exp.business;
        this.editing = exp.status === 'OPENED';
        this.isSending = false;
        this.resetForm();
      },
      (error: HttpErrorResponse) => {
        this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
      }
    );
  }

  async copy() {
    const response = await this.modalService.presentModal(ExpenseCreateComponent, {toCopy: this.expense});
    if (response != null) {
      this.id = response.id;
      this.refreshExpense();
    }
  }

  async presentDetailSort(ev: any) {
    await this.popoverService.presentSort(ev, this.detailSortConfig);
  }

  async presentDiscountSort(ev: any) {
    await this.popoverService.presentSort(ev, this.discountSortConfig);
  }

  save() {
    const toSend: Expense = {
      id: this.expense.id,
      registered: DateService.truncateMillis(this.form.get('registered').value)
    };
    this.isSending = true;
    this.expenseService.edit(
      toSend,
      this.selectedBusiness.id,
      this.selectedCurrency.id,
      this.selectedRate != null ? this.selectedRate.id : null
    ).subscribe(
      (exp: Expense) => {
        this.expense = exp;
        this.selectedCurrency = exp.currency;
        this.selectedBusiness = exp.business;
        this.selectedRate = exp.exchangeRate;
        this.editing = exp.status === 'OPENED';
        this.refreshExpense();
      }, (error: HttpErrorResponse) => {
        this.isSending = false;
        this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
      }
    );
  }

  async delete() {
    const handler = () => {
      this.isSending = true;
      this.expenseService.delete(this.expense.id).subscribe(
        () => {
          this.expense = null;
          this.close();
        }, (error: HttpErrorResponse) => {
          this.isSending = false;
          this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
        }
      );
    };
    await this.actionSheetService.presentDelete('expense.delete', null, 'general.delete', 'general.cancel', handler);
  }

  reset() {
    this.resetForm();
  }

  async searchCurrency() {
    this.selectedCurrency = await this.openSearchCurrency();
    if (this.selectedCurrency == null) {
      this.selectedCurrency = this.expense.currency;
    }
    this.form.get('currency').reset(this.selectedCurrency.name);
  }

  private async openSearchCurrency(): Promise<Currency> {
    return await this.modalService.presentModal(CurrencySearchComponent);
  }

  async searchBusiness() {
    this.selectedBusiness = await this.openSearchBusiness();
    if (this.selectedBusiness == null) {
      this.selectedBusiness = this.expense.currency;
    }
    this.form.get('business').reset(this.selectedBusiness.name);
  }

  private async openSearchBusiness(): Promise<Business> {
    return this.modalService.presentModal(BusinessSearchComponent);
  }

  async searchExchangeRate() {
    this.selectedRate = await this.openSearchExchangeRate();
    if (this.selectedRate == null) {
      this.selectedRate = this.expense.exchangeRate;
    }
    this.form.get('rate').reset(this.calculateRate());
  }

  private async openSearchExchangeRate(): Promise<ExchangeRate> {
    return this.modalService.presentModal(ExchangeRateSearchComponent, {currency: this.selectedCurrency});
  }

  async clearExchangeRate() {
    this.selectedRate = null;
    this.form.get('rate').reset(this.calculateRate());
  }

  close() {
    this.location.back();
  }

}
