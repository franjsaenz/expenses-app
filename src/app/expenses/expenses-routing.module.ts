import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {ExpensesPage} from './expenses.page';
import {AuthService} from '../services/auth.service';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthService],
    component: ExpensesPage
  },
  {
    path: 'detail/:expenseId',
    loadChildren: () => import('./detail/detail.module').then( m => m.DetailPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExpensesPageRoutingModule {}
