import {Component, OnInit, ViewChild} from '@angular/core';
import {Expense} from '../entity/expense';
import {IonInfiniteScroll} from '@ionic/angular';
import {Sort, SortConfig} from '../component/sort-popover/sort-popover.component';
import {ExpenseService} from '../services/expense.service';
import {ErrorMappingService} from '../services/error-mapping.service';
import {Page} from '../entity/page';
import {HttpErrorResponse} from '@angular/common/http';
import {Subscription} from 'rxjs';
import {Business} from '../entity/business';
import {Currency} from '../entity/currency';
import {BusinessSearchComponent} from '../modal/business-search/business-search.component';
import {CurrencySearchComponent} from '../modal/currency-search/currency-search.component';
import {environment} from '../../environments/environment';
import {AuthService} from '../services/auth.service';
import {User} from '../entity/user';
import {Router} from '@angular/router';
import {SettingService} from '../services/setting.service';
import {ExpenseCreateComponent} from '../modal/expense-create/expense-create.component';
import {ModalService} from '../services/modal.service';
import {PopoverService} from '../services/popover.service';
import {LanguageService} from '../services/language.service';

@Component({
  selector: 'app-expenses',
  templateUrl: './expenses.page.html',
  styleUrls: ['./expenses.page.scss'],
})
export class ExpensesPage implements OnInit {

  public dateFormat: string = environment.defaultDateFormat;

  public showFilters = false;

  public loadingContent = true;

  public selectedBusiness: Business;

  public selectedCurrency: Currency;

  public pageNumber = 1;

  public expenses: Array<Expense> = [];

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  public sortConfig: SortConfig;

  constructor(
    private expenseService: ExpenseService,
    private authService: AuthService,
    private errorMappingService: ErrorMappingService,
    private settingService: SettingService,
    private modalService: ModalService,
    private popoverService: PopoverService,
    private languageService: LanguageService,
    private router: Router
  ) {
    languageService.setTitle('expense.plural');
  }

  ngOnInit() {
    this.sortConfig = this.expenseService.getExpenseSortingConfig();
    this.authService.getLoggedIn().then((user: User) => {
      this.dateFormat = this.settingService.getDateFormat(user.dateFormat).pipe;
    });
  }

  ionViewWillEnter() {
    this.expenses = [];
    this.pageNumber = 1;
    this.listContent();
  }

  listContent() {
    this.loadingContent = true;
    if (this.infiniteScroll != null) {
      this.infiniteScroll.disabled = true;
    }
    this.expenseService.search(
      this.selectedCurrency != null ? this.selectedCurrency.id : null,
      this.selectedBusiness != null ? this.selectedBusiness.id : null,
      this.pageNumber - 1,
      this.sortConfig.currentSort.property,
      this.sortConfig.currentSort.ascending
    ).subscribe((resp: Page<Expense>) => {
      this.loadingContent = false;
      this.expenses = this.expenses.concat(resp.content);
      this.pageNumber++;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = resp.last;
    }, (err: HttpErrorResponse) => {
      this.loadingContent = false;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = true;
      this.errorMappingService.displayError(err.error, ErrorMappingService.WARNING);
    });
  }

  resetList() {
    this.selectedBusiness = null;
    this.selectedCurrency = null;
    this.expenses = [];
    this.pageNumber = 1;
  }

  openExpense(exp: Expense) {
    this.router.navigate(['expenses', 'detail', exp.id]);
  }

  async createExpense() {
    const response = await this.modalService.presentModal(ExpenseCreateComponent, {toCopy: null});
    if (response != null) {
      return this.openExpense(response);
    }
  }

  async searchBusiness() {
    this.selectedBusiness = await this.modalService.presentModal(BusinessSearchComponent);
    this.expenses = [];
    this.pageNumber = 1;
    this.listContent();
  }

  clearSelectedBusiness() {
    this.selectedBusiness = null;
    this.expenses = [];
    this.pageNumber = 1;
    this.listContent();
  }

  async searchCurrency() {
    this.selectedCurrency = await this.modalService.presentModal(CurrencySearchComponent);
    this.expenses = [];
    this.pageNumber = 1;
    this.listContent();
  }

  clearSelectedCurrency() {
    this.selectedCurrency = null;
    this.expenses = [];
    this.pageNumber = 1;
    this.listContent();
  }

  async presentSort(ev: any) {
    const sortSub: Subscription = this.expenseService.subscribeExpenseSort((sort: Sort) => {
      this.sortConfig.currentSort = sort;
      this.sortListing();
    });
    await this.popoverService.presentSort(ev, this.sortConfig, sortSub);
  }

  sortListing(): void {
    this.expenses = [];
    this.pageNumber = 1;
    this.listContent();
  }

}
