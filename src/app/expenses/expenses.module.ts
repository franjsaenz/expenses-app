import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {ExpensesPageRoutingModule} from './expenses-routing.module';

import {ExpensesPage} from './expenses.page';
import {DiscountDetailComponent} from '../modal/discount-detail/discount-detail.component';
import {DetailDetailComponent} from '../modal/detail-detail/detail-detail.component';
import {ExpenseCreateComponent} from '../modal/expense-create/expense-create.component';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExpensesPageRoutingModule,
    ReactiveFormsModule,
    TranslateModule
  ],
  declarations: [
    ExpensesPage,
    DetailDetailComponent,
    DiscountDetailComponent,
    ExpenseCreateComponent
  ]
})
export class ExpensesPageModule {
}
