import {Component, OnInit} from '@angular/core';
import {AuthService} from '../services/auth.service';
import {User} from '../entity/user';
import {Router} from '@angular/router';
import {MenuController} from '@ionic/angular';
import {LanguageService} from '../services/language.service';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.page.html',
  styleUrls: ['./not-found.page.scss'],
})
export class NotFoundPage implements OnInit {

  loaded = false;

  loggedIn = false;

  constructor(
    private authService: AuthService,
    private languageService: LanguageService,
    private menuController: MenuController,
    private router: Router
  ) {
    languageService.setTitle('notFound.title');
    this.menuController.enable(false);
    authService.getLoggedIn().then((user: User) => {
      this.loggedIn = user != null;
      this.loaded = true;
    });
  }

  ngOnInit() {
  }

  navigate() {
    if (this.loggedIn) {
      this.menuController.enable(true);
      this.router.navigate(['home'], {replaceUrl: true});
    } else {
      this.router.navigate(['login'], {replaceUrl: true});
    }
  }

}
