import {Component, OnInit} from '@angular/core';

import {MenuController, Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {AuthService} from './services/auth.service';
import {Router, RouterEvent} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {Language, LanguageService} from './services/language.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {

  public currentRoute: string;
  public appPages: Array<MenuItem> = [
    {
      title: 'home.plural',
      icon: 'home',
      url: '/home'
    },
    {
      title: 'budget.plural',
      icon: 'flag',
      url: '/budgets'
    },
    {
      title: 'business.plural',
      icon: 'business',
      url: '/businesses'
    },
    {
      title: 'currency.plural',
      icon: 'cash',
      url: '/currencies',
      hide: true
    },
    {
      title: 'exchangeRate.plural',
      icon: 'trending-up',
      url: '/exchange-rates'
    },
    {
      title: 'category.plural',
      icon: 'albums',
      url: '/categories'
    },
    {
      title: 'product.plural',
      icon: 'pricetags',
      url: '/products'
    },
    {
      title: 'expense.plural',
      icon: 'wallet',
      url: '/expenses'
    },
    {
      title: 'setting.plural',
      icon: 'settings',
      url: '/settings'
    },
    {
      title: 'general.logout',
      icon: 'log-out',
      action: () => {
        this.logout();
      }
    }
  ];

  constructor(
    private authService: AuthService,
    private translateService: TranslateService,
    private languageService: LanguageService,
    private menuController: MenuController,
    private router: Router,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    languageService.setTitle();
    this.initializeApp();
  }

  public ngOnInit() {
  }

  async initializeApp() {
    await this.platform.ready();
    this.statusBar.styleDefault();
    await this.initTranslate();
    this.splashScreen.hide();
    this.router.events.subscribe((event: RouterEvent) => {
      if (event.url != null) {
        this.currentRoute = event.url;
      } else if (this.currentRoute == null) {
        this.currentRoute = '/home';
      }
    });
  }

  private async initTranslate() {
    this.translateService.setDefaultLang('en');
    const def: string = this.translateService.getBrowserLang();
    const using: Language = await this.languageService.getStored();
    await this.translateService.use(using != null ? using.code : (def == null || def.length < 1 ? 'en' : def)).toPromise();
  }

  public logout() {
    this.authService.logout().then(
      () => {
        this.menuController.close();
        this.router.navigate(['login'], {replaceUrl: true});
      }
    );
  }

}

class MenuItem {
  title: string;
  icon: string;
  url?: string;
  action?: () => void;
  hide ? = false;
}
