import {Injectable} from '@angular/core';
import {Exception} from '../entity/exception';
import {TranslateService} from '@ngx-translate/core';
import {ToastController} from '@ionic/angular';
import {ToastOptions} from '@ionic/core';

@Injectable({
  providedIn: 'root'
})
export class ErrorMappingService {

  public static WARNING = 'warning';
  public static SUCCESS = 'success';

  constructor(
    private toastController: ToastController,
    private translateService: TranslateService
  ) {
  }

  public async displayError(error: Exception, toastColor: string, toastDuration: number = null) {
    const key = error != null && error.errorCode != null && error.errorCode.length > 0 ? 'toast.' + error.errorCode : 'toast.UNMAPPED';
    await this.displayToast(key, toastColor, toastDuration);
  }

  public async displayToast(msgCode: string, toastColor: string, toastDuration: number = null) {
    const msg = await this.getErrorMessage(msgCode);
    const close = await this.translateService.get('general.close').toPromise();
    const config: ToastOptions = {
      message: msg,
      position: 'bottom',
      animated: true,
      color: toastColor,
      duration: toastDuration == null ? 0 : toastDuration,
      buttons: [
        {
          text: close,
          role: 'cancel'
        }
      ]
    };
    const toast: HTMLIonToastElement = await this.toastController.create(config);
    toast.present();
  }

  private async getErrorMessage(key: string): Promise<string> {
    const value = await this.translateService.get(key).toPromise();
    if (value === key) {
      return await this.translateService.get('toast.UNMAPPED').toPromise();
    } else {
      return value;
    }
  }

}
