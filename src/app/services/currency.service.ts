import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable, Subject, Subscription} from 'rxjs';
import {Sort, SortConfig} from '../component/sort-popover/sort-popover.component';
import {HttpClient} from '@angular/common/http';
import {Page} from '../entity/page';
import {Currency} from '../entity/currency';
import {Response} from '../entity/response';

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {

  private baseUrl: string = environment.baseUrl + '/currencies';
  private pageSize: number = environment.pageSize;
  private sortingObservable = new Subject<Sort>();

  private currencySortingConfig: SortConfig;

  constructor(private http: HttpClient) {
    this.currencySortingConfig = {
      sortedBy: null,
      ascending: null,
      elements: [{
        title: 'sort.creation',
        property: 'id'
      }, {
        title: 'sort.name',
        property: 'name'
      }, {
        title: 'sort.code',
        property: 'code'
      }],
      currentSort: {
        property: null,
        ascending: null
      },
      publisher: this.publisher()
    };
  }

  public getCurrencySortingConfig(): SortConfig {
    return this.currencySortingConfig;
  }

  public publisher(): Subject<Sort> {
    return this.sortingObservable;
  }

  public subscribeToSort(sub: (sort: Sort) => void): Subscription {
    return this.sortingObservable.asObservable().subscribe(sub);
  }

  public search(term: string, pageNumber: number = 0, sortBy: string = null, direction: boolean = null): Observable<Page<Currency>> {
    const query: { [k: string]: any } = {page: pageNumber, size: this.pageSize};
    if (!(term == null || term === '')) {
      query.term = term;
    }
    if (sortBy != null && sortBy.length > 0) {
      query.sort = sortBy + (direction == null ? '' : (direction ? ',asc' : ',desc'));
    }
    return this.http.get<Page<Currency>>(this.baseUrl, {params: query});
  }

  public create(currency: Currency): Observable<Currency> {
    return this.http.post<Currency>(this.baseUrl, currency);
  }

  public get(id: number): Observable<Currency> {
    return this.http.get<Currency>(this.baseUrl + '/' + id);
  }

  public edit(id: number, currency: Currency): Observable<Currency> {
    return this.http.put<Currency>(this.baseUrl + '/' + id, currency);
  }

  public delete(id: number): Observable<Response> {
    return this.http.delete<Response>(this.baseUrl + '/' + id);
  }

}
