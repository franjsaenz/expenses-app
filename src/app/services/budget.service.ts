import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable, Subject, Subscription} from 'rxjs';
import {Sort, SortConfig} from '../component/sort-popover/sort-popover.component';
import {HttpClient} from '@angular/common/http';
import {Page} from '../entity/page';
import {Budget} from '../entity/budget';
import {Currency} from '../entity/currency';
import {Response} from '../entity/response';
import {BudgetItem} from '../entity/budget-item';
import {Category} from '../entity/category';
import {BudgetReport} from '../entity/budget-report';

@Injectable({
  providedIn: 'root'
})
export class BudgetService {

  private baseUrl: string = environment.baseUrl + '/budgets';
  private itemsUrl = '/items';
  private pageSize: number = environment.pageSize;

  private sortingObservable = new Subject<Sort>();
  private itemSortingObservable = new Subject<Sort>();

  private budgetSortingConfig: SortConfig;
  private itemsSortingConfig: SortConfig;

  constructor(private http: HttpClient) {
    this.budgetSortingConfig = {
      sortedBy: null,
      ascending: null,
      elements: [{
        title: 'sort.creation',
        property: 'id'
      }, {
        title: 'sort.name',
        property: 'name'
      }, {
        title: 'sort.from',
        property: 'from'
      }, {
        title: 'sort.to',
        property: 'to'
      }],
      currentSort: {
        property: 'to',
        ascending: false
      },
      publisher: this.publisher()
    };
    this.itemsSortingConfig = {
      sortedBy: null,
      ascending: null,
      elements: [{
        title: 'sort.creation',
        property: 'id'
      }, {
        title: 'sort.amount',
        property: 'amount'
      }, {
        title: 'sort.category',
        property: 'category.name'
      }],
      currentSort: {
        property: 'category.name',
        ascending: true
      },
      publisher: this.itemSortPublisher()
    };
  }

  public publisher(): Subject<Sort> {
    return this.sortingObservable;
  }

  public subscribeToSort(sub: (sort: Sort) => void): Subscription {
    return this.sortingObservable.asObservable().subscribe(sub);
  }

  public getBudgetSortingConfig(): SortConfig {
    return this.budgetSortingConfig;
  }

  public getItemsSortingConfig(): SortConfig {
    return this.itemsSortingConfig;
  }

  public itemSortPublisher(): Subject<Sort> {
    return this.itemSortingObservable;
  }

  public subscribeToItemSort(sub: (sort: Sort) => void): Subscription {
    return this.itemSortingObservable.asObservable().subscribe(sub);
  }

  public search(
    term: string,
    within: string,
    pageNumber: number = 0,
    sortBy: string = null,
    ascending: boolean = null
  ): Observable<Page<Budget>> {
    const query: { [k: string]: any } = {page: pageNumber, size: this.pageSize};
    if (term != null && term.length > 0) {
      query.term = term;
    }
    if (within != null && within.length > 0) {
      query.within = within;
    }
    if (sortBy != null && sortBy.length > 0) {
      query.sort = sortBy + (ascending == null ? '' : (ascending ? ',asc' : ',desc'));
    }
    return this.http.get<Page<Budget>>(this.baseUrl, {params: query});
  }

  public create(toCreate: Budget, currency: Currency, toCopy: Budget): Observable<Budget> {
    const query: { [k: string]: any } = {currencyId: currency.id};
    if (toCopy != null && toCopy.id > 0) {
      query.originalId = toCopy.id;
    }
    return this.http.post<Budget>(this.baseUrl, toCreate, {params: query});
  }

  public get(budgetId: number): Observable<Budget> {
    const query: { [k: string]: any } = {expanded: false};
    return this.http.get<Budget>(this.baseUrl + '/' + budgetId, {params: query});
  }

  public getExpanded(budgetId: number): Observable<BudgetReport> {
    const query: { [k: string]: any } = {expanded: true};
    return this.http.get<BudgetReport>(this.baseUrl + '/' + budgetId, {params: query});
  }

  public edit(toEdit: Budget, currency: Currency): Observable<Budget> {
    const query: { [k: string]: any } = {currencyId: currency.id};
    return this.http.put<Budget>(this.baseUrl + '/' + toEdit.id, toEdit, {params: query});
  }

  public delete(budgetId: number): Observable<Response> {
    return this.http.delete<Response>(this.baseUrl + '/' + budgetId);
  }

  public listItems(budget: Budget, pageNumber: number = 0, sortBy: string = null, ascending: boolean = null): Observable<Page<BudgetItem>> {
    const query: { [k: string]: any } = {page: pageNumber, size: this.pageSize};
    if (sortBy != null && sortBy.length > 0) {
      query.sort = sortBy + (ascending == null ? '' : (ascending ? ',asc' : ',desc'));
    }
    return this.http.get<Page<BudgetItem>>(this.baseUrl + '/' + budget.id + this.itemsUrl, {params: query});
  }

  public setItem(budget: Budget, category: Category, toSet: BudgetItem): Observable<BudgetItem> {
    const query: { [k: string]: any } = {categoryId: category.id};
    return this.http.post<BudgetItem>(this.baseUrl + '/' + budget.id + this.itemsUrl, toSet, {params: query});
  }

  public deleteItem(budget: Budget, item: BudgetItem): Observable<Response> {
    return this.http.delete<Response>(this.baseUrl + '/' + budget.id + this.itemsUrl + '/' + item.id);
  }

}
