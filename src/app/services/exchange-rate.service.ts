import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable, Subject, Subscription} from 'rxjs';
import {Sort, SortConfig} from '../component/sort-popover/sort-popover.component';
import {HttpClient} from '@angular/common/http';
import {Page} from '../entity/page';
import {ExchangeRate} from '../entity/exchange-rate';
import {Response} from '../entity/response';
import {Currency} from '../entity/currency';

@Injectable({
  providedIn: 'root'
})
export class ExchangeRateService {

  private baseUrl: string = environment.baseUrl + '/exchangeRates';
  private pageSize: number = environment.pageSize;
  private sortingObservable = new Subject<Sort>();

  private exchangeRateSortingConfig: SortConfig;

  constructor(private http: HttpClient) {
    this.exchangeRateSortingConfig = {
      sortedBy: null,
      ascending: null,
      elements: [{
        title: 'sort.creation',
        property: 'id'
      }, {
        title: 'sort.registered',
        property: 'registered'
      }, {
        title: 'sort.selling',
        property: 'from.code'
      }, {
        title: 'sort.buying',
        property: 'to.code'
      }],
      currentSort: {
        property: 'registered',
        ascending: false
      },
      publisher: this.publisher()
    };
  }

  public getExchangeRateSortingConfig(): SortConfig {
    return this.exchangeRateSortingConfig;
  }

  public publisher(): Subject<Sort> {
    return this.sortingObservable;
  }

  public subscribeToSort(sub: (sort: Sort) => void): Subscription {
    return this.sortingObservable.asObservable().subscribe(sub);
  }

  public search(ccy: Currency, pageNumber: number = 0, sortBy: string = null, direction: boolean = null): Observable<Page<ExchangeRate>> {
    const query: { [k: string]: any } = {page: pageNumber, size: this.pageSize};
    if (ccy != null && ccy.id > 0) {
      query.currencyId = ccy.id;
    }
    if (sortBy != null && sortBy.length > 0) {
      query.sort = sortBy + (direction == null ? '' : (direction ? ',asc' : ',desc'));
    }
    return this.http.get<Page<ExchangeRate>>(this.baseUrl, {params: query});
  }

  public create(exchangeRate: ExchangeRate, fromId: number, toId: number): Observable<ExchangeRate> {
    const query: { [k: string]: any } = {};
    query.fromId = fromId;
    query.toId = toId;
    return this.http.post<ExchangeRate>(this.baseUrl, exchangeRate, {params: query});
  }

  public get(id: number): Observable<ExchangeRate> {
    return this.http.get<ExchangeRate>(this.baseUrl + '/' + id);
  }

  public edit(id: number, exchangeRate: ExchangeRate, fromId: number, toId: number): Observable<ExchangeRate> {
    const query: { [k: string]: any } = {};
    query.fromId = fromId;
    query.toId = toId;
    return this.http.put<ExchangeRate>(this.baseUrl + '/' + id, exchangeRate, {params: query});
  }

  public delete(id: number): Observable<Response> {
    return this.http.delete<Response>(this.baseUrl + '/' + id);
  }

}
