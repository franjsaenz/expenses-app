import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Currency} from '../entity/currency';
import {Business} from '../entity/business';
import {Observable} from 'rxjs';
import {Group} from '../entity/group';
import {Product} from '../entity/product';

@Injectable({
  providedIn: 'root'
})
export class StatService {

  private baseUrl: string = environment.baseUrl + '/stats';

  constructor(private http: HttpClient) {
  }

  public byBusiness(currency: Currency, business: Business, fromDate: string, toDate: string): Observable<Group> {
    const query: { [k: string]: any } = {
      currencyId: currency.id,
      businessId: business.id,
      from: fromDate,
      to: toDate
    };
    return this.http.get<Group>(this.baseUrl + '/businesses/totals', {params: query});
  }

  public historyByProduct(currency: Currency, product: Product, fromDate: string, toDate: string): Observable<Group> {
    const query: { [k: string]: any } = {
      currencyId: currency.id,
      productId: product.id,
      from: fromDate,
      to: toDate
    };
    return this.http.get<Group>(this.baseUrl + '/products', {params: query});
  }

}
