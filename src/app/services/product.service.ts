import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable, Subject, Subscription} from 'rxjs';
import {Sort, SortConfig} from '../component/sort-popover/sort-popover.component';
import {HttpClient} from '@angular/common/http';
import {Page} from '../entity/page';
import {Response} from '../entity/response';
import {Product} from '../entity/product';
import {Category} from '../entity/category';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private baseUrl: string = environment.baseUrl + '/products';
  private pageSize: number = environment.pageSize;
  private sortingObservable = new Subject<Sort>();

  private productSortingConfig: SortConfig;

  constructor(private http: HttpClient) {
    this.productSortingConfig = {
      sortedBy: null,
      ascending: null,
      elements: [{
        title: 'sort.creation',
        property: 'id'
      }, {
        title: 'sort.name',
        property: 'name'
      }],
      currentSort: {
        property: null,
        ascending: null
      },
      publisher: this.publisher()
    };
  }

  public getProductSortingConfig(): SortConfig {
    return this.productSortingConfig;
  }

  public publisher(): Subject<Sort> {
    return this.sortingObservable;
  }

  public subscribeToSort(sub: (sort: Sort) => void): Subscription {
    return this.sortingObservable.asObservable().subscribe(sub);
  }

  public search(
    term: string,
    category: Category = null,
    pageNumber: number = 0,
    sortBy: string = null,
    direction: boolean = null
  ): Observable<Page<Product>> {
    const query: { [k: string]: any } = {page: pageNumber, size: this.pageSize};
    if (!(term == null || term === '')) {
      query.term = term;
    }
    if (category != null && category.id > 0) {
      query.categoryId = category.id;
    }
    if (sortBy != null && sortBy.length > 0) {
      query.sort = sortBy + (direction == null ? '' : (direction ? ',asc' : ',desc'));
    }
    return this.http.get<Page<Product>>(this.baseUrl, {params: query});
  }

  public create(product: Product, category: Category): Observable<Product> {
    const query: { [k: string]: any } = {};
    if (category != null && category.id > 0) {
      query.categoryId = category.id;
    }
    return this.http.post<Product>(this.baseUrl, product, {params: query});
  }

  public get(id: number): Observable<Product> {
    return this.http.get<Product>(this.baseUrl + '/' + id);
  }

  public edit(id: number, product: Product, category: Category): Observable<Product> {
    const query: { [k: string]: any } = {};
    if (category != null && category.id > 0) {
      query.categoryId = category.id;
    }
    return this.http.put<Product>(this.baseUrl + '/' + id, product, {params: query});
  }

  public delete(id: number): Observable<Response> {
    return this.http.delete<Response>(this.baseUrl + '/' + id);
  }

}
