import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {User} from '../entity/user';
import {Observable} from 'rxjs';
import {ResetPassword} from '../entity/reset-password';
import {Response} from '../entity/response';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl: string = environment.baseUrl + '/users';

  constructor(private http: HttpClient) {
  }

  public edit(id: number, user: User): Observable<User> {
    return this.http.put<User>(this.baseUrl + '/' + id, user);
  }

  public updatePassword(id: number, password: ResetPassword): Observable<Response> {
    return this.http.put<Response>(this.baseUrl + '/' + id + '/password', password);
  }

}
