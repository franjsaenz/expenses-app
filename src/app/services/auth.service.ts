import {Injectable} from '@angular/core';
import {HttpClient, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {from, Observable} from 'rxjs';
import {switchMap} from 'rxjs/operators';
import {User} from '../entity/user';
import {environment} from '../../environments/environment';
import {Login} from '../entity/login';
import {Storage} from '@capacitor/core';
import {Status} from '../entity/status';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {MenuController} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements HttpInterceptor, CanActivate {

  private userKey = 'LOGGED_USER';

  private baseUrl: string = environment.baseUrl;

  private currentUser: User;

  constructor(
    private menuController: MenuController,
    private http: HttpClient,
    private router: Router
  ) {
  }

  public status(): Observable<Status> {
    return this.http.get<Status>(this.baseUrl);
  }

  public login(login: Login): Observable<User> {
    return this.http.post<User>(this.baseUrl + '/login', login);
  }

  public async storeLoggedIn(user: User, force: boolean = false) {
    const stored: User = await this.getLoggedIn();
    if (force || stored == null || stored.id !== user.id) {
      await Storage.set({
        key: this.userKey,
        value: JSON.stringify(user)
      });
    }
  }

  public async getLoggedIn(): Promise<User> {
    if (this.currentUser != null) {
      return this.currentUser;
    }
    const value = await Storage.get({key: this.userKey});
    return JSON.parse(value.value);
  }

  public async logout() {
    await Storage.clear();
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const promisedUser: Observable<User> = from(this.getLoggedIn());
    return promisedUser.pipe(switchMap((user: User) => {
      let h = req.headers;
      h = h.set('Content-Type', 'application/json');
      if (user != null) {
        const bearer = user.token;
        h = h.set('Authorization', 'Bearer ' + bearer);
      }
      const clonedRequest = req.clone({headers: h});
      return next.handle(clonedRequest);
    }));
  }

  async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    const user: User = await this.getLoggedIn();
    const loggedIn = user != null && user.id > 0;
    if (state.url.includes('login')) {
      if (loggedIn) {
        this.router.navigate(['home'], {replaceUrl: true}).then();
        return false;
      }
    } else {
      if (!loggedIn) {
        this.router.navigate(['login'], {replaceUrl: true}).then();
        return false;
      } else {
        await this.menuController.enable(true);
      }
    }
    return true;
  }

}
