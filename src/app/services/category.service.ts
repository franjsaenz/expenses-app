import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject, Subscription} from 'rxjs';
import {Page} from '../entity/page';
import {Category} from '../entity/category';
import {Sort, SortConfig} from '../component/sort-popover/sort-popover.component';
import {Response} from '../entity/response';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private baseUrl: string = environment.baseUrl + '/categories';
  private pageSize: number = environment.pageSize;
  private sortingObservable = new Subject<Sort>();

  private categorySortingConfig: SortConfig;

  constructor(private http: HttpClient) {
    this.categorySortingConfig = {
      sortedBy: null,
      ascending: null,
      elements: [{
        title: 'sort.creation',
        property: 'id'
      }, {
        title: 'sort.name',
        property: 'name'
      }],
      currentSort: {
        property: null,
        ascending: null
      },
      publisher: this.publisher()
    };
  }

  public getCategorySortingConfig(): SortConfig {
    return this.categorySortingConfig;
  }

  public publisher(): Subject<Sort> {
    return this.sortingObservable;
  }

  public subscribeToSort(sub: (sort: Sort) => void): Subscription {
    return this.sortingObservable.asObservable().subscribe(sub);
  }

  public search(term: string, pageNumber: number = 0, sortBy: string = null, direction: boolean = null): Observable<Page<Category>> {
    const query: { [k: string]: any } = {page: pageNumber, size: this.pageSize};
    if (!(term == null || term === '')) {
      query.term = term;
    }
    if (sortBy != null && sortBy.length > 0) {
      query.sort = sortBy + (direction == null ? '' : (direction ? ',asc' : ',desc'));
    }
    return this.http.get<Page<Category>>(this.baseUrl, {params: query});
  }

  public create(category: Category): Observable<Category> {
    return this.http.post<Category>(this.baseUrl, category);
  }

  public get(id: number): Observable<Category> {
    return this.http.get<Category>(this.baseUrl + '/' + id);
  }

  public edit(id: number, category: Category): Observable<Category> {
    return this.http.put<Category>(this.baseUrl + '/' + id, category);
  }

  public delete(id: number): Observable<Response> {
    return this.http.delete<Response>(this.baseUrl + '/' + id);
  }

}
