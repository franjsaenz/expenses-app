import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DateService {

  constructor() {
  }

  public static truncateMillis(val: string): string {
    if (val == null || val.length < 1) {
      return null;
    }
    try {
      const date: Date = new Date(val);
      date.setMilliseconds(0);
      return date.toISOString();
    } catch (e) {
      console.error(e);
      return null;
    }
  }

}
