import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable, Subject, Subscription} from 'rxjs';
import {Sort, SortConfig} from '../component/sort-popover/sort-popover.component';
import {HttpClient} from '@angular/common/http';
import {Page} from '../entity/page';
import {Expense} from '../entity/expense';
import {Detail} from '../entity/detail';
import {Discount} from '../entity/discount';
import {Response} from '../entity/response';
import {Product} from '../entity/product';
import {Category} from '../entity/category';
import {Business} from '../entity/business';
import {Currency} from '../entity/currency';
import {ExchangeRate} from '../entity/exchange-rate';

@Injectable({
  providedIn: 'root'
})
export class ExpenseService {

  private baseUrl: string = environment.baseUrl + '/expenses';
  private detailUrl = '/details';
  private discountUrl = '/discounts';
  private pageSize: number = environment.pageSize;

  private expenseSortingConfig: SortConfig;
  private detailsSortingConfig: SortConfig;
  private discountSortingConfig: SortConfig;

  private expenseSortingObservable = new Subject<Sort>();
  private detailSortingObservable = new Subject<Sort>();
  private discountSortingObservable = new Subject<Sort>();

  constructor(private http: HttpClient) {
    this.expenseSortingConfig = {
      sortedBy: null,
      ascending: null,
      elements: [{
        title: 'sort.creation',
        property: 'id'
      }, {
        title: 'sort.registered',
        property: 'registered'
      }, {
        title: 'sort.business',
        property: 'business.name'
      }],
      currentSort: {
        property: null,
        ascending: null
      },
      publisher: this.expenseSortPublisher()
    };
    this.detailsSortingConfig = {
      sortedBy: null,
      ascending: null,
      elements: [{
        title: 'sort.creation',
        property: 'id'
      }, {
        title: 'sort.amount',
        property: 'amount'
      }, {
        title: 'sort.product',
        property: 'product.name'
      }, {
        title: 'sort.category',
        property: 'category.name'
      }],
      currentSort: {
        property: null,
        ascending: null
      },
      publisher: this.detailSortPublisher()
    };
    this.discountSortingConfig = {
      sortedBy: null,
      ascending: null,
      elements: [{
        title: 'sort.creation',
        property: 'id'
      }, {
        title: 'sort.amount',
        property: 'amount'
      }],
      currentSort: {
        property: null,
        ascending: null
      },
      publisher: this.discountSortPublisher()
    };
  }

  public getExpenseSortingConfig(): SortConfig {
    return this.expenseSortingConfig;
  }

  public expenseSortPublisher(): Subject<Sort> {
    return this.expenseSortingObservable;
  }

  public subscribeExpenseSort(sub: (sort: Sort) => void): Subscription {
    return this.expenseSortingObservable.asObservable().subscribe(sub);
  }

  public detailSortPublisher(): Subject<Sort> {
    return this.detailSortingObservable;
  }

  public subscribeDetailSort(sub: (sort: Sort) => void): Subscription {
    return this.detailSortingObservable.asObservable().subscribe(sub);
  }

  public getDetailsSortingConfig(): SortConfig {
    return this.detailsSortingConfig;
  }

  public discountSortPublisher(): Subject<Sort> {
    return this.discountSortingObservable;
  }

  public subscribeDiscountSort(sub: (sort: Sort) => void): Subscription {
    return this.discountSortingObservable.asObservable().subscribe(sub);
  }

  public getDiscountSortingConfig(): SortConfig {
    return this.discountSortingConfig;
  }

  public search(
    currencyId: number,
    businessId: number,
    pageNumber: number = 0,
    sortBy: string = null,
    direction: boolean = null
  ): Observable<Page<Expense>> {
    const query: { [k: string]: any } = {page: pageNumber, size: this.pageSize};
    if (currencyId != null && currencyId > 0) {
      query.currencyId = currencyId;
    }
    if (businessId != null && businessId > 0) {
      query.businessId = businessId;
    }
    if (sortBy != null && sortBy.length > 0) {
      query.sort = sortBy + (direction == null ? '' : (direction ? ',asc' : ',desc'));
    }
    return this.http.get<Page<Expense>>(this.baseUrl, {params: query});
  }

  public create(toSend: Expense, business: Business, currency: Currency, rate: ExchangeRate, toCopy: Expense = null): Observable<Expense> {
    const query: { [k: string]: any } = {businessId: business.id, currencyId: currency.id};
    if (rate != null && rate.id > 0) {
      query.exchangeRateId = rate.id;
    }
    if (toCopy != null) {
      query.originalId = toCopy.id;
    }
    return this.http.post<Expense>(this.baseUrl, toSend, {params: query});
  }

  public get(id: number): Observable<Expense> {
    return this.http.get<Expense>(this.baseUrl + '/' + id);
  }

  public edit(expense: Expense, newBusinessId: number, newCurrencyId: number, newExchangeRateId: number = null): Observable<Expense> {
    const query: { [k: string]: any } = {businessId: newBusinessId, currencyId: newCurrencyId};
    if (newExchangeRateId != null && newExchangeRateId > 0) {
      query.exchangeRateId = newExchangeRateId;
    }
    return this.http.put<Expense>(this.baseUrl + '/' + expense.id, {registered: expense.registered}, {params: query});
  }

  public delete(id: number): Observable<Response> {
    return this.http.delete<Response>(this.baseUrl + '/' + id);
  }

  public updateStatus(id: number, newStatus: string): Observable<Expense> {
    const query: { [k: string]: any } = {status: newStatus, extended: true};
    return this.http.put<Expense>(this.baseUrl + '/' + id + '/status', null, {params: query});
  }

  public listDetails(
    expenseId: number,
    pageNumber: number = 0,
    sortBy: string = null,
    direction: boolean = null
  ): Observable<Page<Detail>> {
    const query: { [k: string]: any } = {page: pageNumber, size: this.pageSize};
    if (sortBy != null && sortBy.length > 0) {
      query.sort = sortBy + (direction == null ? '' : (direction ? ',asc' : ',desc'));
    }
    return this.http.get<Page<Detail>>(this.baseUrl + '/' + expenseId + this.detailUrl, {params: query});
  }

  public createDetail(
    expenseId: number,
    discount: Discount,
    product: Product,
    category: Category = null,
    rep: number
  ): Observable<Response> {
    const query: { [k: string]: any } = {productId: product.id};
    if (category != null) {
      query.categoryId = category.id;
    }
    if (rep != null && rep > 0) {
      query.repetitions = rep;
    }
    return this.http.post<Response>(this.baseUrl + '/' + expenseId + this.detailUrl, discount, {params: query});
  }

  public editDetail(expenseId: number, discount: Discount, product: Product, category: Category = null): Observable<Detail> {
    const query: { [k: string]: any } = {productId: product.id};
    if (category != null) {
      query.categoryId = category.id;
    }
    return this.http.put<Detail>(this.baseUrl + '/' + expenseId + this.detailUrl + '/' + discount.id, discount, {params: query});
  }

  public deleteDetail(expenseId: number, discount: Discount): Observable<Response> {
    return this.http.delete<Response>(this.baseUrl + '/' + expenseId + this.detailUrl + '/' + discount.id);
  }

  public listDiscounts(
    expenseId: number,
    pageNumber: number = 0,
    sortBy: string = null,
    direction: boolean = null
  ): Observable<Page<Discount>> {
    const query: { [k: string]: any } = {page: pageNumber, size: this.pageSize};
    if (sortBy != null && sortBy.length > 0) {
      query.sort = sortBy + (direction == null ? '' : (direction ? ',asc' : ',desc'));
    }
    return this.http.get<Page<Discount>>(this.baseUrl + '/' + expenseId + this.discountUrl, {params: query});
  }

  public createDiscount(expenseId: number, discount: Discount): Observable<Discount> {
    return this.http.post<Discount>(this.baseUrl + '/' + expenseId + this.discountUrl, discount);
  }

  public editDiscount(expenseId: number, discount: Discount): Observable<Discount> {
    return this.http.put<Discount>(this.baseUrl + '/' + expenseId + this.discountUrl + '/' + discount.id, discount);
  }

  public deleteDiscount(expenseId: number, discount: Discount): Observable<Response> {
    return this.http.delete<Response>(this.baseUrl + '/' + expenseId + this.discountUrl + '/' + discount.id);
  }

}
