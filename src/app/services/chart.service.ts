import {ElementRef, Injectable} from '@angular/core';
import {Chart} from 'chart.js';
import {BudgetReport} from '../entity/budget-report';
import {BudgetReportItem} from '../entity/budget-report-item';
import {Group} from '../entity/group';
import {GroupDetail} from '../entity/group-detail';
import {TranslateService} from '@ngx-translate/core';
import {Platform} from '@ionic/angular';
import {SettingService} from './setting.service';

@Injectable({
  providedIn: 'root'
})
export class ChartService {

  private budgetMaxSize = 1.2;

  constructor(
    private platform: Platform,
    private translateService: TranslateService,
    private settingService: SettingService
  ) {
  }

  public async createBudgetChart(report: BudgetReport, canvas: ElementRef): Promise<Chart> {
    const maxSize: number = await this.settingService.getSetting(SettingService.budgetChartMaxSize);
    if (maxSize != null && maxSize > 0) {
      this.budgetMaxSize = maxSize;
    }
    const dataLabel = [];
    const datasetData = [];
    const datasetBackground = [];
    const startDate = new Date(report.budget.from).getTime();
    const endDate = new Date(report.budget.to).getTime();
    const nowDate = new Date().getTime();
    let completed = 0;
    if (endDate < nowDate) {
      completed = 1;
    } else if (startDate > nowDate) {
      completed = 0;
    } else {
      completed = (nowDate - startDate) / (endDate - startDate);
    }

    report.details.forEach((d: BudgetReportItem) => {
      dataLabel.push(d.name);
      datasetData.push(d.completed > this.budgetMaxSize ? this.budgetMaxSize : d.completed);
      datasetBackground.push(this.getColorForBudget(d.completed, completed));
    });
    dataLabel.push(this.translateService.instant('general.total'));
    datasetData.push(report.totalCompleted > this.budgetMaxSize ? this.budgetMaxSize : report.totalCompleted);
    datasetBackground.push(this.getColorForBudget(report.totalCompleted, completed));
    dataLabel.push(this.translateService.instant('general.chart.periodElapsed'));
    datasetData.push(completed > this.budgetMaxSize ? this.budgetMaxSize : completed.toFixed(2));
    datasetBackground.push(this.getColorForBudget(completed, null));

    const data = {
      labels: dataLabel,
      datasets: [
        {
          data: datasetData,
          backgroundColor: datasetBackground
        }
      ]
    };
    const ops = {
      responsive: true,
      maintainAspectRatio: false,
      legend: {
        display: false
      },
      title: {
        display: false
      },
      scales: {
        xAxes: [{
          display: true,
          scaleLabel: {
            display: true,
            labelString: this.translateService.instant('general.chart.completed')
          }
        }],
        yAxes: [{
          display: this.platform.width() >= 1280
        }]
      }
    };
    return this.createChart(canvas, 'horizontalBar', data, ops);
  }

  public createBusinessChart(stat: Group, canvas: ElementRef): Chart {
    const dataLabel = [];
    const datasetData = [];
    const datasetBackground = [];
    stat.details.forEach((d: GroupDetail, i: number) => {
      dataLabel.push(d.name);
      datasetData.push(d.total);
      datasetBackground.push(this.rainbow(stat.details.length, i + 1));
    });

    const data = {
      labels: dataLabel,
      datasets: [
        {
          data: datasetData,
          backgroundColor: datasetBackground
        }
      ]
    };
    const ops = {
      responsive: true,
      maintainAspectRatio: false,
      legend: {
        display: false
      },
      title: {
        display: false
      },
      scales: {
        xAxes: [{
          display: true,
          scaleLabel: {
            display: true,
            labelString: this.translateService.instant('general.chart.expended')
          }
        }],
        yAxes: [{
          display: this.platform.width() >= 1280,
          scaleLabel: {
            display: false
          }
        }]
      }
    };
    return this.createChart(canvas, 'horizontalBar', data, ops);
  }

  public createProductChart(stat: Group, canvas: ElementRef): Chart {
    const totals = {
      label: this.translateService.instant('general.chart.discountsApplied'),
      fill: false,
      borderColor: this.rainbow(32, 20),
      data: []
    };
    const subtotals = {
      label: 'Total',
      fill: false,
      borderColor: this.rainbow(32, 1),
      data: []
    };
    stat.details.forEach((det) => {
      totals.data.push({x: det.date, y: det.total});
      subtotals.data.push({x: det.date, y: det.subtotal});
    });
    const data = {
      datasets: [subtotals, totals]
    };
    const ops = {
      responsive: true,
      maintainAspectRatio: false,
      title: {
        display: false
      },
      elements: {
        line: {
          tension: 0.15
        }
      },
      tooltips: {
        mode: 'index',
        intersect: false,
        callbacks: {
          footer: (items, ignored) => {
            let i = -1;
            items.forEach((it) => {
              i = it.index;
            });
            if (i < 0 || i >= stat.details.length) {
              return '';
            }
            return stat.details[i].name;
          }
        }
      },
      scales: {
        xAxes: [{
          type: 'time',
          time: {
            unit: 'month'
          },
          scaleLabel: {
            display: true,
            labelString: this.translateService.instant('general.chart.date')
          }
        }],
        yAxes: [{
          scaleLabel: {
            display: true,
            labelString: this.translateService.instant('general.amount')
          },
          ticks: {
            beginAtZero: true
          }
        }]
      },
    };
    return this.createChart(canvas, 'line', data, ops);
  }

  private createChart(canvas: ElementRef, chartType: string, graphData: any, graphOption): Chart {
    return new Chart(canvas.nativeElement, {
      type: chartType,
      data: graphData,
      options: graphOption
    });
  }

  private getColorForBudget(completed, elapsed) {
    let c;
    if (completed > this.budgetMaxSize) {
      c = '#000000';
    } else if (completed > 1) {
      c = '#c0392b';
    } else if (completed === 1) {
      c = '#2980b9';
    } else if (elapsed != null && elapsed >= 0 && elapsed < 1) {
      if (completed < elapsed) {
        c = '#2ecc71';
      } else if (completed === elapsed) {
        c = '#27ae60';
      } else {
        c = '#f1c40f';
      }
    } else if (completed > 0.9) {
      c = '#f39c12';
    } else if (completed > 0.65) {
      c = '#f1c40f';
    } else if (completed > 0.33) {
      c = '#27ae60';
    } else {
      c = '#2ecc71';
    }
    return c;
  }

  /* tslint:disable:no-bitwise */
  rainbow(numOfSteps, step): string {
    let r;
    let g;
    let b;
    const h = step / numOfSteps;
    const i = ~~(h * 6);
    const f = h * 6 - i;
    const q = 1 - f;
    switch (i % 6) {
      case 0:
        r = 1;
        g = f;
        b = 0;
        break;
      case 1:
        r = q;
        g = 1;
        b = 0;
        break;
      case 2:
        r = 0;
        g = 1;
        b = f;
        break;
      case 3:
        r = 0;
        g = q;
        b = 1;
        break;
      case 4:
        r = f;
        g = 0;
        b = 1;
        break;
      case 5:
        r = 1;
        g = 0;
        b = q;
        break;
    }
    return '#' + ('00' + (~~(r * 255)).toString(16)).slice(-2) +
      ('00' + (~~(g * 255)).toString(16)).slice(-2) +
      ('00' + (~~(b * 255)).toString(16)).slice(-2);
  }

  /* tslint:enable:no-bitwise */

}
