import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable, Subject, Subscription} from 'rxjs';
import {Sort, SortConfig} from '../component/sort-popover/sort-popover.component';
import {HttpClient} from '@angular/common/http';
import {Page} from '../entity/page';
import {Business} from '../entity/business';
import {Response} from '../entity/response';

@Injectable({
  providedIn: 'root'
})
export class BusinessService {

  private baseUrl: string = environment.baseUrl + '/businesses';
  private pageSize: number = environment.pageSize;
  private sortingObservable = new Subject<Sort>();

  private businessSortingConfig: SortConfig;

  constructor(private http: HttpClient) {
    this.businessSortingConfig = {
      sortedBy: null,
      ascending: null,
      elements: [{
        title: 'sort.creation',
        property: 'id'
      }, {
        title: 'sort.name',
        property: 'name'
      }],
      currentSort: {
        property: null,
        ascending: null
      },
      publisher: this.publisher()
    };
  }

  public getBusinessSortingConfig(): SortConfig {
    return this.businessSortingConfig;
  }

  public publisher(): Subject<Sort> {
    return this.sortingObservable;
  }

  public subscribeToSort(sub: (sort: Sort) => void): Subscription {
    return this.sortingObservable.asObservable().subscribe(sub);
  }

  public search(term: string, pageNumber: number = 0, sortBy: string = null, ascending: boolean = null): Observable<Page<Business>> {
    const query: { [k: string]: any } = {page: pageNumber, size: this.pageSize};
    if (term != null && term.length > 0) {
      query.term = term;
    }
    if (sortBy != null && sortBy.length > 0) {
      query.sort = sortBy + (ascending == null ? '' : (ascending ? ',asc' : ',desc'));
    }
    return this.http.get<Page<Business>>(this.baseUrl, {params: query});
  }

  public create(business: Business): Observable<Business> {
    return this.http.post<Business>(this.baseUrl, business);
  }

  public get(id: number): Observable<Business> {
    return this.http.get<Business>(this.baseUrl + '/' + id);
  }

  public edit(id: number, business: Business): Observable<Business> {
    return this.http.put<Business>(this.baseUrl + '/' + id, business);
  }

  public delete(id: number): Observable<Response> {
    return this.http.delete<Response>(this.baseUrl + '/' + id);
  }

}
