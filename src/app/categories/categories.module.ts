import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {CategoriesPageRoutingModule} from './categories-routing.module';

import {CategoriesPage} from './categories.page';
import {FlexLayoutModule} from '@angular/flex-layout';
import {CategoryDetailComponent} from '../modal/category-detail/category-detail.component';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CategoriesPageRoutingModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    TranslateModule
  ],
  declarations: [CategoriesPage, CategoryDetailComponent]
})
export class CategoriesPageModule {}
