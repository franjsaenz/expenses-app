import {Component, OnInit, ViewChild} from '@angular/core';
import {CategoryService} from '../services/category.service';
import {Page} from '../entity/page';
import {Category} from '../entity/category';
import {HttpErrorResponse} from '@angular/common/http';
import {IonInfiniteScroll} from '@ionic/angular';
import {ErrorMappingService} from '../services/error-mapping.service';
import {Sort, SortConfig} from '../component/sort-popover/sort-popover.component';
import {Subscription} from 'rxjs';
import {CategoryDetailComponent} from '../modal/category-detail/category-detail.component';
import {ModalService} from '../services/modal.service';
import {PopoverService} from '../services/popover.service';
import {LanguageService} from '../services/language.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.page.html',
  styleUrls: ['./categories.page.scss'],
})
export class CategoriesPage implements OnInit {

  public loadingContent = true;

  public searchTerm: string;

  public pageNumber = 1;

  public categories: Array<Category> = [];

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  public sortConfig: SortConfig;

  constructor(
    private categoriesService: CategoryService,
    private errorMappingService: ErrorMappingService,
    private modalService: ModalService,
    private popoverService: PopoverService,
    private languageService: LanguageService
  ) {
    languageService.setTitle('category.plural');
  }

  ngOnInit() {
    this.sortConfig = this.categoriesService.getCategorySortingConfig();
    this.listContent();
  }

  listContent() {
    this.loadingContent = true;
    if (this.infiniteScroll != null) {
      this.infiniteScroll.disabled = true;
    }
    this.categoriesService.search(
      this.searchTerm, this.pageNumber - 1,
      this.sortConfig.currentSort.property,
      this.sortConfig.currentSort.ascending
    ).subscribe((resp: Page<Category>) => {
      this.loadingContent = false;
      this.categories = this.categories.concat(resp.content);
      this.pageNumber++;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = resp.last;
    }, (err: HttpErrorResponse) => {
      this.loadingContent = false;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = true;
      this.errorMappingService.displayError(err.error, ErrorMappingService.WARNING);
    });
  }

  search(term: string) {
    this.resetList();
    if (term == null || term.length < 1) {
      this.searchTerm = null;
    } else {
      this.searchTerm = term;
    }
    this.listContent();
  }

  resetList() {
    this.searchTerm = null;
    this.categories = [];
    this.pageNumber = 1;
  }

  async openCategory(cat: Category, index: number) {
    const response = await this.modalService.presentModal(CategoryDetailComponent, {category: cat});
    if (response == null || index == null || index < 0) {
      this.categories = [];
      this.pageNumber = 1;
      this.listContent();
    } else {
      this.categories[index] = response;
    }
  }

  async presentSort(ev: any) {
    const sortSub: Subscription = this.categoriesService.subscribeToSort((sort: Sort) => {
      this.sortConfig.currentSort = sort;
      this.sortListing();
    });
    await this.popoverService.presentSort(ev, this.sortConfig, sortSub);
  }

  sortListing(): void {
    this.categories = [];
    this.pageNumber = 1;
    this.listContent();
  }

}
