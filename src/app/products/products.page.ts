import {Component, OnInit, ViewChild} from '@angular/core';
import {ErrorMappingService} from '../services/error-mapping.service';
import {IonInfiniteScroll} from '@ionic/angular';
import {ProductService} from '../services/product.service';
import {Sort, SortConfig} from '../component/sort-popover/sort-popover.component';
import {Page} from '../entity/page';
import {HttpErrorResponse} from '@angular/common/http';
import {Product} from '../entity/product';
import {ProductDetailComponent} from '../modal/product-detail/product-detail.component';
import {ModalService} from '../services/modal.service';
import {PopoverService} from '../services/popover.service';
import {LanguageService} from '../services/language.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})
export class ProductsPage implements OnInit {

  public loadingContent = true;

  public searchTerm: string;

  public pageNumber = 1;

  public products: Array<Product> = [];

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  public sortConfig: SortConfig;

  constructor(
    private productService: ProductService,
    private errorMappingService: ErrorMappingService,
    private languageService: LanguageService,
    private modalService: ModalService,
    private popoverService: PopoverService
  ) {
    languageService.setTitle('product.plural');
  }

  ngOnInit() {
    this.sortConfig = this.productService.getProductSortingConfig();
    this.listContent();
  }

  listContent() {
    this.loadingContent = true;
    if (this.infiniteScroll != null) {
      this.infiniteScroll.disabled = true;
    }
    this.productService.search(
      this.searchTerm, null, this.pageNumber - 1,
      this.sortConfig.currentSort.property,
      this.sortConfig.currentSort.ascending
    ).subscribe((resp: Page<Product>) => {
      this.loadingContent = false;
      this.products = this.products.concat(resp.content);
      this.pageNumber++;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = resp.last;
    }, (err: HttpErrorResponse) => {
      this.loadingContent = false;
      this.infiniteScroll.complete();
      this.infiniteScroll.disabled = true;
      this.errorMappingService.displayError(err.error, ErrorMappingService.WARNING);
    });
  }

  search(term: string) {
    this.resetList();
    if (term == null || term.length < 1) {
      this.searchTerm = null;
    } else {
      this.searchTerm = term;
    }
    this.listContent();
  }

  resetList() {
    this.searchTerm = null;
    this.products = [];
    this.pageNumber = 1;
  }

  async openProduct(prod: Product, index: number) {
    const response = await this.modalService.presentModal(ProductDetailComponent, {product: prod});
    if (response == null || index == null || index < 0) {
      this.products = [];
      this.pageNumber = 1;
      this.listContent();
    } else {
      this.products[index] = response;
    }
  }

  async presentSort(ev: Event) {
    await this.popoverService.presentSort(ev, this.sortConfig, this.productService.subscribeToSort((sort: Sort) => {
      this.sortConfig.currentSort = sort;
      this.sortListing();
    }));
  }

  sortListing(): void {
    this.products = [];
    this.pageNumber = 1;
    this.listContent();
  }

}
