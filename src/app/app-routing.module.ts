import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {AuthService} from './services/auth.service';

const routes: Routes = [
  {
    path: 'home',
    canActivate: [AuthService],
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'login',
    canActivate: [AuthService],
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'categories',
    canActivate: [AuthService],
    loadChildren: () => import('./categories/categories.module').then( m => m.CategoriesPageModule)
  },
  {
    path: 'products',
    canActivate: [AuthService],
    loadChildren: () => import('./products/products.module').then( m => m.ProductsPageModule)
  },
  {
    path: 'businesses',
    canActivate: [AuthService],
    loadChildren: () => import('./businesses/businesses.module').then( m => m.BusinessesPageModule)
  },
  {
    path: 'currencies',
    canActivate: [AuthService],
    loadChildren: () => import('./currencies/currencies.module').then( m => m.CurrenciesPageModule)
  },
  {
    path: 'exchange-rates',
    loadChildren: () => import('./exchange-rates/exchange-rates.module').then( m => m.ExchangeRatesPageModule)
  },
  {
    path: 'expenses',
    canActivate: [AuthService],
    loadChildren: () => import('./expenses/expenses.module').then( m => m.ExpensesPageModule)
  },
  {
    path: 'settings',
    canActivate: [AuthService],
    loadChildren: () => import('./settings/settings.module').then( m => m.SettingsPageModule)
  },
  {
    path: 'budgets',
    canActivate: [AuthService],
    loadChildren: () => import('./budgets/budgets.module').then( m => m.BudgetsPageModule)
  },
  {
    path: '**',
    loadChildren: () => import('./not-found/not-found.module').then( m => m.NotFoundPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
