import {Component, OnInit} from '@angular/core';
import {AuthService} from '../services/auth.service';
import {UserService} from '../services/user.service';
import {ErrorMappingService} from '../services/error-mapping.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {User} from '../entity/user';
import {HttpErrorResponse} from '@angular/common/http';
import {ResetPassword} from '../entity/reset-password';
import {DateFormat, SettingService} from '../services/setting.service';
import {environment} from '../../environments/environment';
import {SwUpdate} from '@angular/service-worker';
import {CurrencySearchComponent} from '../modal/currency-search/currency-search.component';
import {Currency} from '../entity/currency';
import {ModalService} from '../services/modal.service';
import {Language, LanguageService} from '../services/language.service';
import {ActionSheetService} from '../services/action-sheet.service';
import {MenuController} from '@ionic/angular';
import {Status} from '../entity/status';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  version: string = environment.version;

  sending = false;

  updating = false;

  passwordForm: FormGroup;

  status: Status;

  user: User;

  defaultCurrency: Currency;

  budgetMaxSize = 1.2;

  budgetMaxSizeTouched = false;

  now: Date = new Date();

  dateFormats: DateFormat[];

  languages: Array<Language>;

  defaultLang: Language;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private errorMappingService: ErrorMappingService,
    private settingService: SettingService,
    private languageService: LanguageService,
    private modalService: ModalService,
    private actionSheetService: ActionSheetService,
    private menuController: MenuController,
    private formBuilder: FormBuilder,
    private swUpdate: SwUpdate
  ) {
    languageService.setTitle('setting.title');
    this.dateFormats = settingService.getDateFormats();
    this.languages = languageService.getLanguages();
  }

  ngOnInit() {
    this.authService.status().subscribe((status: Status) => {
      this.status = status;
      console.log(this.status);
    });
    this.authService.getLoggedIn().then((user: User) => {
      this.user = user;
    });
    this.settingService.getSetting(SettingService.defaultCurrencyKey).then(
      (currency: Currency) => {
        if (currency != null && currency.id > 0) {
          this.defaultCurrency = currency;
        }
      }
    );
    this.settingService.getSetting(SettingService.budgetChartMaxSize).then(
      (maxSize: number) => {
        if (maxSize != null && maxSize > 0) {
          this.budgetMaxSize = maxSize;
        }
      }
    );
    this.languageService.getStored().then((defLang: Language) => {
      this.defaultLang = defLang;
    });
    this.initPasswordForm();
  }

  updateDefaultLang(lang: Language) {
    this.defaultLang = null;
    this.languageService.useLanguage(lang).then(() => {
      setTimeout(() => {
        this.defaultLang = lang;
      }, 500);
    });
  }

  async refreshPage() {
    await this.actionSheetService.presentUpdate(
      'setting.refreshTitle', 'setting.refreshSubtitle',
      'setting.update', 'general.cancel',
      () => {
        this.refreshPageHandler().then();
      }
    );
  }

  private async refreshPageHandler() {
    await this.menuController.enable(false);
    this.sending = true;
    this.updating = true;
    if (this.swUpdate.isEnabled) {
      await this.swUpdate.checkForUpdate();
    }
    await this.menuController.enable(true);
    document.location.reload();
  }

  async searchCurrency() {
    const response = await this.modalService.presentModal(CurrencySearchComponent);
    if (response != null) {
      this.defaultCurrency = response;
      await this.settingService.storeSetting(SettingService.defaultCurrencyKey, this.defaultCurrency);
    }
  }

  async clearCurrency() {
    this.defaultCurrency = null;
    await this.settingService.storeSetting(SettingService.defaultCurrencyKey, this.defaultCurrency);
  }

  async setBudgetMaxSize() {
    await this.settingService.storeSetting(SettingService.budgetChartMaxSize, this.budgetMaxSize);
    this.budgetMaxSizeTouched = false;
  }

  private initPasswordForm() {
    this.passwordForm = this.formBuilder.group({
      oldPassword: new FormControl('', [
        Validators.required
      ]),
      newPassword: new FormControl('', [
        Validators.required
      ]),
      confirmPassword: new FormControl('', [
        Validators.required
      ])
    }, {
      validators: confirmPasswordValidator('newPassword', 'confirmPassword')
    });
  }

  private sendingData(): () => void {
    this.sending = true;
    this.passwordForm.disable();
    return () => {
      this.sending = false;
      this.passwordForm.reset();
      this.passwordForm.enable();
    };
  }

  updateDateFormat(format: string) {
    const u: User = {
      zoneId: this.user.zoneId,
      dateFormat: format
    };
    const send: () => void = this.sendingData();
    this.userService.edit(this.user.id, u).subscribe(
      (user: User) => {
        user.token = this.user.token;
        this.user = user;
        send();
        this.authService.storeLoggedIn(this.user, true).then();
        this.errorMappingService.displayToast('toast.dateFormat', ErrorMappingService.SUCCESS, 2000);
      }, (error: HttpErrorResponse) => {
        send();
        this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
      }
    );
  }

  updatePassword() {
    const pass: ResetPassword = {
      oldPassword: this.passwordForm.get('oldPassword').value,
      newPassword: this.passwordForm.get('newPassword').value
    };
    const send: () => void = this.sendingData();
    this.userService.updatePassword(this.user.id, pass).subscribe(
      () => {
        send();
        this.errorMappingService.displayToast('toast.password', ErrorMappingService.SUCCESS, 2000);
      }, (error: HttpErrorResponse) => {
        send();
        this.errorMappingService.displayError(error.error, ErrorMappingService.WARNING);
      }
    );
  }

}

export function confirmPasswordValidator(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];
    if (matchingControl.errors && !matchingControl.errors.confirmPasswordValidator) {
      return;
    }
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({confirmPasswordValidator: true});
    } else {
      matchingControl.setErrors(null);
    }
  };
}
