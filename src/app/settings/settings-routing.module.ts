import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {SettingsPage} from './settings.page';
import {AuthService} from '../services/auth.service';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthService],
    component: SettingsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SettingsPageRoutingModule {}
