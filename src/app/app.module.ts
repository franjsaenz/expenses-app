import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';

import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {AuthService} from './services/auth.service';
import {BudgetSearchComponent} from './modal/budget-search/budget-search.component';
import {BusinessSearchComponent} from './modal/business-search/business-search.component';
import {CategorySearchComponent} from './modal/category-search/category-search.component';
import {CurrencySearchComponent} from './modal/currency-search/currency-search.component';
import {ExchangeRateSearchComponent} from './modal/exchange-rate-search/exchange-rate-search.component';
import {ProductSearchComponent} from './modal/product-search/product-search.component';
import {SortPopoverComponent} from './component/sort-popover/sort-popover.component';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CopyTextComponent} from './modal/copy-text/copy-text.component';

@NgModule({
  declarations: [
    AppComponent,
    SortPopoverComponent,
    BudgetSearchComponent,
    BusinessSearchComponent,
    CategorySearchComponent,
    CurrencySearchComponent,
    ExchangeRateSearchComponent,
    ProductSearchComponent,
    CopyTextComponent
  ],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
    {provide: HTTP_INTERCEPTORS, useClass: AuthService, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}
