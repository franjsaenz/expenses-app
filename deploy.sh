set -e

ionic build --prod
zip -r app.zip www
scp app.zip frani@fsquiroz.com:~/expenses.zip
rm app.zip
rm -rf www

ssh fsquiroz bash /home/frani/deploy-expenses
